package TypeAndSymbol;

import AST.TemType;
import AST.VarDecl;

/**
 * Created by fzy on 17-4-3.
 */
public class VariableType extends Type {
    public int dimension;
    public boolean isConstant = false;
    public VarDecl varDecl;

    public VariableType(TemType temType, VarDecl _varDecl){
        if (temType != null){
            name = temType.name;
            dimension = temType.dimension;
        }
        varDecl = _varDecl;
    }

    public VariableType(VariableType temType, VarDecl _varDecl, boolean _isConstant){
        if (temType != null){
            name = temType.name;
            dimension = temType.dimension;
        }
        varDecl = _varDecl;
        isConstant = _isConstant;
    }

    public VariableType(String _name, int _dimension, VarDecl _varDecl, boolean _isConstant){
        name = _name;
        dimension = _dimension;
        varDecl = _varDecl;
        isConstant = _isConstant;
    }

    public boolean equal(VariableType other){
        return name.equals(other.name)  && dimension == other.dimension;
    }

    public boolean equal(TemType other){
        return name.equals(other.name)  && dimension == other.dimension;
    }

    public boolean isBuiltIn(){return name.equals("int")  || name.equals("bool")  || name.equals("string");}

    public boolean isPrimitive(){
        return (name.equals("int")  || name.equals("bool")  || name.equals("string")) && dimension == 0;
    }

    public boolean checkInt(){
        return name.equals("int") && dimension == 0;
    }
}

