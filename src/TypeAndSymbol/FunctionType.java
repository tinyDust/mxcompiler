package TypeAndSymbol;

import AST.ConsDecl;
import AST.FuncDecl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fzy on 17-4-3.
 */
public class FunctionType extends Type{
    public VariableType returnType;
    public List<VariableType> parameters = new ArrayList<>();

    public FunctionType(FuncDecl node){
        name = node.funcName;
        returnType = new VariableType(node.temtype, null);
        if (node.parameters != null)
            node.parameters.forEach(x -> parameters.add(new VariableType(x.temType, null)));
    }

    public FunctionType(ConsDecl node){
        name = node.funcName;
        returnType = null;
        if (node.parameters != null)
            node.parameters.forEach(x -> parameters.add(new VariableType(x.temType, null)));
    }

    //for all one dimension
    public FunctionType(String _returnType, String _parameter){
        returnType = new VariableType(_returnType, 0, null, true);
        if (_parameter != null) parameters.add(new VariableType(_parameter, 0, null,true));
    }

    public FunctionType(String _returnType, String _parameter1, String _parameter2){
        returnType = new VariableType(_returnType, 0, null,true);
        if (_parameter1 != null) parameters.add(new VariableType(_parameter1, 0, null,true));
        if (_parameter2 != null) parameters.add(new VariableType(_parameter2, 0, null,true));
    }
    public FunctionType(){}
}