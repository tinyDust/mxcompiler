package TypeAndSymbol;

import AST.VarDecl;
import ASTVisitor.CheckerVisitor;
import ASTVisitor.environment;
import ASTVisitor.wrapper;
import IRClass.Entity;
import IRClass.Var;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fzy on 17-4-4.
 */
public class Scope {
    public Map<String, Type> vars;
    public Scope parent;
    public LinkedList<Scope> children = new LinkedList<>();
    public environment envir;
    public HashMap<String, Integer> variableOffset;//for class use
    public HashMap<Integer, Var> localVariables = new HashMap<>();//for func use

    public Scope(Scope _parent, environment _envir){
        vars = new HashMap<>();
        parent = _parent;
        if (_parent != null)
            _parent.children.push(this);
        envir = _envir;
    }

    public void refresh(){
        variableOffset = new HashMap<>();
        Integer offset = 0;
        for (Map.Entry s : vars.entrySet()){
            if (s.getValue() instanceof VariableType) {
                variableOffset.put((String) s.getKey(), offset);
                offset += 8;
            }
        }
    }

    public Var getLocal(int id){
        Scope tem = this;
        Var var;
        while(tem != null){
            var = tem.localVariables.get(id);
            if (var != null) return var;
            tem = tem.parent;
        }
        System.out.println("nonexist variable in getLocal");
        return null;
    }

    public void initLocal(){
        for (Map.Entry s : vars.entrySet()){
            if (! (s.getValue() instanceof VariableType))  { System.out.println(s.getKey() + "is not a variableType in initlocal");}
            else {
                localVariables.put(((VariableType) s.getValue()).varDecl.id,
                        new Var(IRClass.Type.INT64, new Entity((String) s.getKey())));
            }
        }

        if (children != null)
            for (Scope scope: children) scope.initLocal();
    }

    public void put(String key, Type value){
        if (vars.get(key) != null) {
            System.out.println("redefined variable or function " + key);
            System.exit(1);
        }
        vars.put(key, value);
    }

    public Type get(String key, wrapper wrp){
        Scope tem = this;
        while(tem.vars.get(key) == null) {
            if (tem.parent == null) return null;
            tem = tem.parent;
        }
        if (wrp != null)
            wrp.envir = tem.envir;
        return tem.vars.get(key);
    }

    public void show(int d){
        System.out.println("Scope Dimension " + d);
        if (vars != null)
            for (Map.Entry<String, Type> entry:vars.entrySet()){
                System.out.println("Key = " + entry.getKey());
            }

        if (children != null){
            children.forEach(x -> x.show(d + 1));
        }
    }

    public boolean hasKey(String key){
        return vars.get(key) != null;
    }
}
