package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public abstract class Expr extends IR {
    public abstract void accept(IRVisitor visitor);
    public abstract Operand asmValue();
}
