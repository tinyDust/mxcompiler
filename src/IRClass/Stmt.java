package IRClass;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public abstract class Stmt extends IR {
    public abstract void accept(IRVisitor visitor);
}
