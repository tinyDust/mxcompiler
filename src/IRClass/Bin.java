package IRClass;
import AST.BinaryExpr.BinaryOp;
import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Bin extends Expr {
    public BinaryOp op;
    public Expr lhs, rhs;

    public Bin(BinaryOp _op, Expr _lhs, Expr _rhs){
        op = _op;
        lhs = _lhs;
        rhs = _rhs;
        type = rhs.type;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
