package IRClass;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Assign extends Stmt {
    public Expr lhs, rhs;
    public Assign(Expr _lhs, Expr _rhs){
        lhs = _lhs;
        rhs = _rhs;
        type = _rhs.type;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
}
