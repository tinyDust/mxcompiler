package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-15.
 */
public class Ref extends Expr {
    public DefinedVariable tmp;
    public Ref(DefinedVariable _tmp){tmp = _tmp;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
