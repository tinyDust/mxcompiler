package IRClass;
import AST.UnaryExpr.UnaryOps;
import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Uni extends Expr {
    public UnaryOps op;
    public Expr expr;
    public Uni(UnaryOps _op, Expr _expr){ op = _op; expr = _expr; type = expr.type;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
