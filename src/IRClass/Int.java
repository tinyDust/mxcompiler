package IRClass;

import Asm.Operand.Operand;

import static IRClass.Type.INT32;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Int extends Expr {
    public int value;
    public Int(int _value){type = INT32; value = _value;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
