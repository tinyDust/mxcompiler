package IRClass;
import Asm.Assembly.Label;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class CJump extends Stmt {
    public Expr condition;
    public Label thenlabel, elselabel;

    public CJump(Expr _condition, Label _thenlabel, Label _elselabel){
        condition = _condition;
        thenlabel = _thenlabel;
        elselabel = _elselabel;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
}
