package IRClass;

import Asm.Operand.DirectMemoryReference;
import Asm.Operand.IndirectMemoryReference;
import Asm.Operand.MemoryReference;
import Asm.Operand.Operand;

import static IRClass.Type.INT64;
import IRVisitor.IRVisitor;

/**
 * Created by john on 17-5-9.
 */
public class Var extends Expr {
    public Entity entity;

    public Var(Type _type, Entity _entity){type = _type; entity = _entity;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
    public void setMemRef(IndirectMemoryReference _memref){ entity.memref = _memref;}
    public void setMemRef(DirectMemoryReference _addr){entity.addr = _addr;}
}
