package IRClass;

import Asm.Assembly.Label;
import Asm.Literal.Symbol;
import Asm.Operand.Operand;

import java.util.LinkedList;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Call extends Expr {
    public Label label;
    public LinkedList<Expr> args;
    public Call(Label _label, LinkedList<Expr> _args){
        label = _label;
        args = _args;
    }

    public void accept(IRVisitor visitor){visitor.visit(this);}

    public Operand asmValue(){return null;}
}
