package IRClass;

import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
abstract public class IR {
    public Type type;
    public abstract void accept(IRVisitor visitor);
}
