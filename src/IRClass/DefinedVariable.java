package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-14.
 */
public class DefinedVariable extends Expr {
    static int id = 0;
    public int myId;
    public Type type;
    public DefinedVariable(Type _type){
        type = _type;
        myId = id;
        id = id + 1;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
