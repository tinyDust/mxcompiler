package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-13.
 */
public class bool extends Expr {
    public boolean value;

    public bool(boolean _value) {
        type = Type.INT8;
        value = _value;
    }

    public void accept(IRVisitor visitor){visitor.visit(this);}

    public Operand asmValue(){return null;}
}
