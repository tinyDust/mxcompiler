package IRClass;
import Asm.Assembly.Label;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class LabelStmt extends Stmt {
    public Label label;
    public LabelStmt(Label _label){label = _label;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
}
