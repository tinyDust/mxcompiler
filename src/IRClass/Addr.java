package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Addr extends Expr {
    public Entity entity;
    public Addr(Type _type, Entity _entity){
        type = _type;
        entity = _entity;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
