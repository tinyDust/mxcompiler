package IRClass;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Return extends Stmt {
    public Expr expr;
    public Return(Expr _expr){expr = _expr;}
    public void accept(IRVisitor visitor){visitor.visit(this);}}
