package IRClass;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class ExprStmt extends Stmt {
    public Expr expr;
    public ExprStmt(Expr _expr){expr = _expr;}
    public void accept(IRVisitor visitor){visitor.visit(this);}
}
