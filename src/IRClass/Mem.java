package IRClass;

import Asm.Operand.Operand;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Mem extends Expr {
    public Expr expr;
    public Mem(Expr _expr){
        expr = _expr;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return null;}
}
