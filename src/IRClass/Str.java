package IRClass;

import Asm.Assembly.Label;
import Asm.Operand.AbsoluteAddress;
import Asm.Operand.DirectMemoryReference;
import Asm.Operand.Operand;

import static IRClass.Type.INT64;
import IRVisitor.IRVisitor;
/**
 * Created by john on 17-5-9.
 */
public class Str extends Expr {
    public Label label;
    public String s;
    public Str(String _s, Label _label){
        type = INT64;
        s = _s;
        label = _label;
    }
    public void accept(IRVisitor visitor){visitor.visit(this);}
    public Operand asmValue(){return new AbsoluteAddress(label);}
}
