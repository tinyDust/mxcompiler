package IRClass;

import Asm.Literal.Symbol;
import Asm.Operand.DirectMemoryReference;
import Asm.Operand.IndirectMemoryReference;
import Asm.Operand.MemoryReference;

/**
 * Created by john on 17-5-18.
 */
public class Entity {
    public String name;
    public DirectMemoryReference addr;
    public IndirectMemoryReference memref;

    public Entity(String _name){name = _name; }
}
