package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class StringExpr extends Expr{
    public String myName = "StringExpr";
    public String content;
    public StringExpr(String _content){
        content = _content.substring(1, _content.length() - 1);
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
