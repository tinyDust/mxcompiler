package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-4-3.
 */
public class LogicalExpr extends Expr {
    public String myName = "LogicalExpr";
    public boolean value;
    public LogicalExpr(boolean _value) {value = _value;}
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
