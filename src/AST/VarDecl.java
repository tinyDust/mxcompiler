package AST;

import ASTVisitor.IASTVisitor;
import Asm.Operand.MemoryReference;

import java.util.LinkedList;

/**
 * Created by fzy on 17-3-31.
 */
public class VarDecl extends Decl{
    public static int counter;
    public int id;
    public String myName = "VarDecl";
    public TemType temType;
    public String name;
    public Expr expr;
    public LinkedList<IRClass.Stmt> varDeclStmts;

    public VarDecl(TemType _temtype, String _name, Expr _expr){
        temType = _temtype;
        name = _name;
        expr = _expr;
        id = counter++;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
