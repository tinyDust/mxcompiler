package AST;

import ASTVisitor.IASTVisitor;
import TypeAndSymbol.Scope;

/**
 * Created by fzy on 17-3-31.
 */
public abstract class ASTnode {
    public String myName = "ASTnode";
    public Scope scope;
    public abstract Object accept(IASTVisitor visitor);
}
