package AST;

import ASTVisitor.IASTVisitor;
import IRClass.IR;
import IRClass.Var;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class FuncDecl extends Decl {
    public String myName = "FuncDecl";
    public TemType temtype;
    public String funcName;
    public LinkedList<VarDecl> parameters;
    public BlockStmt blockStmt;

    public LinkedList<IRClass.Stmt> irs;
//    public LinkedList<Var> parametersIR = new LinkedList<>();
    public int savedReg = 0;

    public void initParameters(){
        for (VarDecl varDecl: parameters){
            Var var = scope.localVariables.get(varDecl.name);
            scope.localVariables.remove(varDecl.name);
        }
    }

    public FuncDecl(TemType _temtype, String _funcName, LinkedList<VarDecl> _parameters, BlockStmt _blockStmt){
        temtype = _temtype;
        funcName = _funcName;
        parameters = _parameters;
        blockStmt = _blockStmt;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }

    public boolean inlineable(){
        return blockStmt.stmts.size() == 1 && blockStmt.stmts.get(0) instanceof JumpStmt && funcName.equals("func");
    }

    public void setIR(LinkedList<IRClass.Stmt> _irs){
        irs = _irs;
    }
}
