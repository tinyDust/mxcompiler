package AST;

import ASTVisitor.IASTVisitor;
import TypeAndSymbol.Scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class ClassDecl extends Decl{
    public String myName = "ClassDecl";
    public String className;
    public LinkedList<VarDecl> vars;
    public LinkedList<FuncDecl> funcs;
    public ConsDecl cons;
    public int size;

    public ClassDecl(String _className, LinkedList<VarDecl> _vars, LinkedList<FuncDecl> _funcs, ConsDecl _cons){
        className = _className;
        vars = _vars;
        funcs = _funcs;
        cons = _cons;
        size = vars.size() * 8;
    }

    public ClassDecl(Scope _scope){
        scope = _scope;
        size = 8;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
