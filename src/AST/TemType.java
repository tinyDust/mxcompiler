package AST;

import static IRClass.Type.*;

/**
 * Created by fzy on 17-4-3.
 */
public class TemType {
    public String name;
    public int dimension;
    public IRClass.Type type;
    private int getDimension(String whole){
        int len = whole.length(), d = 0;
        for (int i = len - 1; i >= 0; --i){
            if (whole.charAt(i) == '[') ++d;
        }
        return d;
    }

    private void setType(){
        if (dimension > 0) type = INT64;
        else if (name.equals("int")) type = INT32;
        else if (name.equals("bool")) type = INT8;
        else type = INT64;
    }

    public TemType(String _name, int _dimension){
        name = _name;
        dimension = _dimension;
        setType();
    }

    public TemType(String _name, String whole){
        name = _name;
        dimension = getDimension(whole);
        setType();
    }

    public boolean isPrimitive(){
        return (name.equals("int")  || name.equals("bool")  || name.equals("string")) && dimension == 0;
    }
}
