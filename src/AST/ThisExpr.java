package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class ThisExpr extends Expr {
    public String myName = "ThisExpr";
    public String className = null;
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
