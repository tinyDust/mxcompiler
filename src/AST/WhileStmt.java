package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class WhileStmt extends Stmt{
    public String myName = "WhileStmt";
    public Expr condition;
    public Stmt stmt;
    public WhileStmt(Expr _condition, Stmt _stmt){
        condition = _condition;
        stmt = _stmt;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
