package AST;

import ASTVisitor.IASTVisitor;

import static AST.UnaryExpr.UnaryOps.MMINUS;
import static AST.UnaryExpr.UnaryOps.PPLUS;

/**
 * Created by fzy on 17-3-31.
 */

public class PostUnaryExpr extends Expr {
    public String myName = "PostUnaryExpr";
    public UnaryExpr.UnaryOps op;
    public Expr expr;

    UnaryExpr.UnaryOps getOp(String _op){
        switch(_op){
            case "++": return PPLUS;
            case "--": return MMINUS;
            default:System.out.println("Error PostUnaryExpr");return PPLUS;
        }
    }

    public PostUnaryExpr(String _op, Expr _expr){
        op = getOp(_op);
        expr = _expr;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
