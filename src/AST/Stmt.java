package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
abstract public class Stmt extends ASTnode{
    public String myName = "Stmt";
    public abstract Object accept(IASTVisitor visitor);
}
