package AST;

import ASTVisitor.IASTVisitor;
import IRClass.Str;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class Program extends ASTnode {
    public String myName = "Program";
    public LinkedList<VarDecl> varDecls = new LinkedList<>();
    public LinkedList<ClassDecl> classDecls = new LinkedList<>();
    public LinkedList<FuncDecl> funcDecls = new LinkedList<>();
    public LinkedList<Decl> Decls = new LinkedList<>();
    public LinkedList<Str> GlobalStringList = new LinkedList<>();
    public HashMap<String, Decl> allfuncs = new HashMap<>();
    public FuncDecl initFunc = new FuncDecl(null, "initGlobalVariables", null, null);
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
