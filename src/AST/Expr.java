package AST;

import ASTVisitor.IASTVisitor;
import TypeAndSymbol.Type;

/**
 * Created by fzy on 17-3-31.
 */
abstract public class Expr extends Stmt{
    public String myName = "Expr";
    public String asFuncName = null;
    public Type returnType;
    public abstract Object accept(IASTVisitor visitor);
}
