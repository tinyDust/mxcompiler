package AST;

import ASTVisitor.IASTVisitor;

import static AST.BinaryExpr.BinaryOp.*;

/**
 * Created by fzy on 17-3-31.
 */
public class BinaryExpr extends Expr {
    public String myName = "BinaryExpr";
    public enum BinaryOp {
        ASSIGN,
        LOGICAL_OR, LOGICAL_AND,
        BITWISE_OR, BITWISE_AND, XOR,
        EQ, NE, LT, GT, LE, GE,
        SHL, SHR,
        ADD, SUB,
        MUL, DIV, MOD
    }

    public Expr lhs, rhs;
    public BinaryOp op;

    private BinaryOp change(String _op){
        switch (_op) {
            case "*": return MUL;
            case "/": return DIV;
            case "%": return MOD;
            case "+": return ADD;
            case "-": return SUB;
            case "<<": return SHL;
            case ">>": return SHR;
            case ">": return GT;
            case "<": return LT;
            case ">=": return GE;
            case "<=": return LE;
            case "==": return EQ;
            case "!=": return NE;
            case "&": return BITWISE_AND;
            case "|": return BITWISE_OR;
            case "^": return XOR;
            case "&&": return LOGICAL_AND;
            case "||": return LOGICAL_OR;
            case "=": return ASSIGN;
            default: System.out.println("Error Type"); return ASSIGN;
        }
    }

    public BinaryExpr(Expr _lhs, Expr _rhs, String _op){
        lhs = _lhs;
        rhs = _rhs;
        op = change(_op);
    }

    public BinaryExpr(Expr _lhs, Expr _rhs, BinaryOp _op){
        lhs = _lhs;
        rhs = _rhs;
        op = _op;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
