package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class IntExpr extends Expr {
    public String myName = "IntExpr";
    public int number;
    public IntExpr(int _number) {
        number = _number;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
