package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-4-2.
 */
public class EmptyStmt extends Stmt {
    public String myName = "EmptyStmt";
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
