package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-4-3.
 */
public class MemberAccess extends Expr{
    public String myName = "MemberAccess";
    public Expr expr;
    public String name;
    public Integer memberOffset = null;
    public String asFuncName = null;
    public MemberAccess(Expr _expr, String _name){
        expr = _expr;
        name = _name;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
