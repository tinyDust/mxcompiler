package AST;

import ASTVisitor.IASTVisitor;
import IRClass.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-4-2.
 */
public class ConsDecl extends Decl {
    public String myName = "ConsDecl";
    public String funcName;
    public LinkedList<VarDecl> parameters;
    public BlockStmt blockStmt;
    public LinkedList<IRClass.Stmt> irs;
//    public LinkedList<Var> parametersIR = new LinkedList<>();
    public int savedReg = 0;

    public void initParameters(){
 //       parametersIR = new LinkedList<>();
        for (VarDecl varDecl: parameters){
            Var var = scope.localVariables.get(varDecl.name);
   //         parametersIR.add(var);
            scope.localVariables.remove(varDecl.name);
        }
    }

    public ConsDecl(String _funcName, LinkedList<VarDecl> _parameters, BlockStmt _blockStmt){
        funcName = _funcName;
        parameters = _parameters;
        blockStmt = _blockStmt;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }

    public void setIR(LinkedList<IRClass.Stmt> _irs){
        irs = _irs;
    }
}
