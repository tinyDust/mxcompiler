package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class IfStmt extends Stmt {
    public String myName = "IfStmt";
    public Expr condition;
    public Stmt stmt, elseStmt;
    public IfStmt(Expr _condition, Stmt _stmt, Stmt _elseStmt){
        condition = _condition;
        stmt = _stmt;
        elseStmt = _elseStmt;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
