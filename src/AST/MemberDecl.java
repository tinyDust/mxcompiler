package AST;

import ASTVisitor.IASTVisitor;

import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class MemberDecl extends Decl {
    public String myName = "MemberDecl";
    public List<VarDecl> vars;
    public List<FuncDecl> funcs;
    FuncDecl constructor;
    public MemberDecl(List<VarDecl> _vars, List<FuncDecl> _funcs, FuncDecl _constructor){
        vars = _vars;
        funcs = _funcs;
        constructor = _constructor;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
