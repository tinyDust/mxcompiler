package AST;

import ASTVisitor.IASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class FunctionCallExpr extends Expr {
    public String myName = "FuncCall";
    public Expr functionName;
    public LinkedList<Expr> parameters;
    public FunctionCallExpr(Expr _functionName, LinkedList<Expr> _parameters){
        functionName = _functionName;
        parameters = _parameters;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
