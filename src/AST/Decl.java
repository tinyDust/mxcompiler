package AST;

import ASTVisitor.IASTVisitor;
import IRClass.IR;
/**
 * Created by fzy on 17-3-31.
 */
abstract public class Decl extends ASTnode{
    public String myName = "Decl";
    public IR ir;
    public abstract Object accept(IASTVisitor visitor);
}
