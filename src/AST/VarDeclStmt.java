package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class VarDeclStmt extends Stmt {
    public String myName = "VarDeclStmt";
    public VarDecl varDecl;
    public VarDeclStmt(VarDecl _varDecl){
        varDecl = _varDecl;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
