package AST;

import ASTVisitor.IASTVisitor;
import TypeAndSymbol.Scope;
import TypeAndSymbol.Type;

/**
 * Created by fzy on 17-3-31.
 */
public class IdentifierExpr extends Expr {
    public String myName = "Identifier";
    public String Identifier;
    public String className;
    public Scope classScope;
    public Type type;
    public boolean isClassMember = false;
    public IdentifierExpr(String s){
        Identifier = s;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
