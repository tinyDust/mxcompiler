package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class NullExpr extends Expr {
    public String myName = "NullExpr";
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
