package AST;

import ASTVisitor.IASTVisitor;

import java.util.List;

/**
 * Created by fzy on 17-3-31.
 */
public class NewExpr extends Expr {
    public String myName = "NewExpr";
    public TemType temType;
    public List<Expr> size;

    public NewExpr(TemType _temType, List<Expr> _size){
        temType = _temType;
        size = _size;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}