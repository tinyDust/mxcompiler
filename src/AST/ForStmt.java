package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-4-3.
 */
public class ForStmt extends Stmt{
    public String myName = "forStmt";
    public Expr init, condi, step;
    public Stmt stmt;
    public ForStmt(Expr _init, Expr _condi, Expr _step, Stmt _stmt){
        init = _init;
        condi = _condi;
        step = _step;
        stmt = _stmt;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
