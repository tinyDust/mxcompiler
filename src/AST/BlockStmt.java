package AST;

import ASTVisitor.IASTVisitor;

import java.util.List;

/**
 * Created by fzy on 17-4-2.
 */
public class BlockStmt extends Stmt {
    public String myName = "BlockStmt";
    public List<Stmt> stmts;
    public BlockStmt(List<Stmt> _stmts){stmts = _stmts;}
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
