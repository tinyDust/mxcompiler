package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class IndexAccessExpr extends Expr {
    public String myName = "IndexAccess";
    public Expr name;
    public Expr index;
    public IndexAccessExpr(Expr _name, Expr _index){
        name = _name;
        index = _index;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
