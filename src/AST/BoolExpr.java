package AST;

import ASTVisitor.IASTVisitor;

/**
 * Created by fzy on 17-3-31.
 */
public class BoolExpr extends Expr {
    public String myName = "BoolExpr";
    public boolean value;
    public BoolExpr(boolean _value){
        value = _value;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
