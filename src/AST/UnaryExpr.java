package AST;

import ASTVisitor.IASTVisitor;

import static AST.UnaryExpr.UnaryOps.*;

/**
 * Created by fzy on 17-3-31.
 */
public class UnaryExpr extends Expr {
    public String myName = "UnaryExpr";
    public enum UnaryOps{
        PPLUS, MMINUS,
        PLUS, MINUS,
        BANG, SIM
    }
    public UnaryOps op;
    public Expr expr;

    UnaryOps getOp(String _op){
        switch(_op){
            case "++": return PPLUS;
            case "--": return MMINUS;
            case "+": return PLUS;
            case "-": return MINUS;
            case "!": return BANG;
            case "~": return SIM;
            default:System.out.println("Error UnaryExpr");return PLUS;
        }
    }

    public UnaryExpr(String _op, Expr _expr){
        op = getOp(_op);
        expr = _expr;
    }

    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
