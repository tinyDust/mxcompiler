package AST;

import ASTVisitor.IASTVisitor;
import TypeAndSymbol.VariableType;

import static AST.JumpStmt.jumps.*;

/**
 * Created by fzy on 17-4-1.
 */
public class JumpStmt extends Stmt {
    public String myName = "JumpStmt";
    public enum jumps{
        BREAK, CONTINUE, RETURN
    }
    public jumps jump;
    public Expr expr=null;
    public VariableType returnType;
    public JumpStmt(String _jump, Expr _expr){
        switch(_jump){
            case "break"    : jump = BREAK; break;
            case "continue" : jump = CONTINUE; break;
            case "return"   : jump = RETURN; break;
            default         : System.out.println("Error type in JumpStmt");
        }
        expr = _expr;
    }
    public Object accept(IASTVisitor visitor){
        return visitor.visit(this);
    }
}
