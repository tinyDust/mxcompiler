package test;

import ASTVisitor.*;
import IRVisitor.CodeGenerator;
import IRVisitor.IRPrintVisitor;
import IRVisitor.optimizer;
import LexerParser.MxLexer;
import LexerParser.MxParser;
import LexerParser.SyntaxErrorListerner;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
/**
 * Created by fzy on 17-3-30.
 */
public class test {
    public static void main(String[] args)throws IOException {
        InputStream is = new FileInputStream("program.txt"); // or System.in;
        ANTLRInputStream input = new ANTLRInputStream(is);
        MxLexer lexer = new MxLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MxParser parser = new MxParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(new SyntaxErrorListerner());

        ParseTree tree = parser.program(); // program is the starting rule
        ParseTreeWalker walker = new ParseTreeWalker();
        ASTCreator astCreator = new ASTCreator();
        walker.walk(astCreator, tree);

//        PrintVisitor pt = new PrintVisitor();
//        pt.visit(astCreator.program);

        ResolverVisitor res = new ResolverVisitor();
        res.visit(astCreator.program);

//        CheckerVisitor chk = new CheckerVisitor(res);
//        chk.visit(astCreator.program);

        IRGeneratorVisitor irg = new IRGeneratorVisitor(res);
        irg.generate(astCreator.program);

        optimizer optimizer = new optimizer(astCreator.program);
        optimizer.BinaryOptimize();

//        IRPrintVisitor irpt = new IRPrintVisitor();
//        irpt.start(optimizer.program);

        CodeGenerator cg = new CodeGenerator();
        cg.generate(optimizer.program);
        cg.show();

        printHeader();

    }

    private static void printHeader() throws IOException {
        File f = new File("head.txt");
        FileInputStream fis = new FileInputStream(f);
        char ch;
        for (int i = 0; i < f.length(); i++) {
            ch = (char) fis.read();
            System.out.print(ch);
        }
        fis.close();
    }
}
