package test;

import ASTVisitor.ASTCreator;
import ASTVisitor.PrintVisitor;
import LexerParser.MxLexer;
import LexerParser.MxParser;
import LexerParser.SyntaxErrorListerner;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

/**
 * Created by fzy on 17-4-4.
 */



public class test2 {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("program.txt"); // or System.in;
        ANTLRInputStream input = new ANTLRInputStream(is);
        MxLexer lexer = new MxLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MxParser parser = new MxParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(new SyntaxErrorListerner());

        ParseTree tree = parser.program(); // program is the starting rule
        ParseTreeWalker walker = new ParseTreeWalker();
        ASTCreator astCreator = new ASTCreator();
        walker.walk(astCreator, tree);


        PrintVisitor pt = new PrintVisitor();
        pt.visit(astCreator.program);

    }
}
