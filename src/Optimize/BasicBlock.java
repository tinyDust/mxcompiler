package Optimize;

import IRClass.Stmt;

import java.util.LinkedList;

/**
 * Created by john on 17-5-29.
 */
public class BasicBlock {
    public LinkedList<Stmt> blockContent;
    public LinkedList<BasicBlock> pred, succ;
    public LinkedList<String> succLabelName;

    public BasicBlock(){
        blockContent = new LinkedList<>();
        pred = new LinkedList<>();
        succ = new LinkedList<>();
    }

    public void add(Stmt stmt){
        blockContent.add(stmt);
    }

    public int size(){
        return blockContent.size();
    }

}
