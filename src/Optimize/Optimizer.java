package Optimize;

import AST.ClassDecl;
import AST.FuncDecl;
import AST.Program;

import java.util.LinkedList;

/**
 * Created by john on 17-5-29.
 */
public class Optimizer {
    public LinkedList<Function> funcs;

    public Optimizer(){
        funcs = new LinkedList<>();
    }

    public void generate(Program program){
        for (FuncDecl func: program.funcDecls){
            funcs.add(new Function(func.irs));
        }

        for (ClassDecl classDecl: program.classDecls){
            for (FuncDecl func : classDecl.funcs){
                funcs.add(new Function(func.irs));
            }

            if (classDecl.cons != null){
                funcs.add(new Function(classDecl.cons.irs));
            }
        }

        for (Function function : funcs) function.createBasicBlock();
    }
}
