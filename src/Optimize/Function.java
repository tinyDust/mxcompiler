package Optimize;

import Asm.Assembly.Label;
import IRClass.*;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by john on 17-5-29.
 */
public class Function {
    public LinkedList<Stmt> stmts;
    public LinkedList<BasicBlock> basicBlocks;

    private BasicBlock currentBasicBlock;
    private HashMap<String, BasicBlock> blockHashMap;

    public Function(LinkedList<Stmt> _stmts){
        stmts = _stmts;
        blockHashMap = new HashMap<>();
        currentBasicBlock = null;
    }

    public void createBasicBlock(){
        BasicBlock EpilogueBlock = new BasicBlock();
        Label endLabel = new Label();
//        EpilogueBlock.add(new Return(endLabel));
        for (Stmt stmt : stmts){
            currentBasicBlock.add(stmt);
            if (stmt instanceof Jump){
                currentBasicBlock.succLabelName.add(((Jump) stmt).label.name);
                basicBlocks.add(currentBasicBlock);
                currentBasicBlock = new BasicBlock();
            }
            else if (stmt instanceof  CJump){
                currentBasicBlock.succLabelName.add(((CJump) stmt).thenlabel.name);
                currentBasicBlock.succLabelName.add(((CJump) stmt).elselabel.name);
                basicBlocks.add(currentBasicBlock);
                currentBasicBlock = new BasicBlock();
            }
            else if (stmt instanceof LabelStmt){
                blockHashMap.put(((LabelStmt) stmt).label.name, currentBasicBlock);
            }
            else if (stmt instanceof Return){

            }
        }
    }
}
