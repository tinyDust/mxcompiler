grammar Mx;

program
    :   programSection* EOF
    ;

programSection
    :   functionDeclaration # FuncDecl
    |   classDeclaration    # ClassDecl
    |   variableDeclaration';' # VarDecl
    ;

classDeclaration
    :   'class' Identifier '{' memberDeclaration* '}'
    ;

memberDeclaration
    :   functionDeclaration     # MemFunc
    |   constructorDeclaration  # MemCons
    |   variableDeclaration';'     # MemVar
    ;

functionDeclaration
    :   (type|'void') Identifier '('typeParameterList?')' blockStatement
    ;

constructorDeclaration
    :   Identifier '('typeParameterList?')' blockStatement
    ;

type
    :   primitiveType (lop='['rop=']')*
    ;

primitiveType
    :   'bool'
    |   'int'
    |   'string'
    |   Identifier
    ;

parameterList
    :   expression (',' expression)*
    ;

typeParameterList
    :   variableDeclaration (',' variableDeclaration)*
    ;

blockStatement
    :   '{' statement* '}'
    ;

variableDeclaration
    :   type Identifier ('=' expression)?
    ;

statement
    :   expression';'           # ExprStmt
    |   variableDeclarationStmt # VarDeclStmt
    |   conditionStatement      # ConditionStmt
    |   loopStatement           # LoopStmt
    |   jumpStatement           # JumpStmt
    |   blockStatement          # BlockStmt
    |   ';'                     # EmptyStmt
    ;

variableDeclarationStmt
    : variableDeclaration';'
    ;

conditionStatement
    :   'if' '('expression')'
            statement
        (
        'else'
        statement
        )?
    ;

loopStatement
    :   'while' '('expression')' statement          # While
    |   'for' '('
        init=expression? ';' cond=expression? ';' step=expression?
        ')'
        statement                                   # For
    ;

jumpStatement
    :   name='continue'';'
    |   name='break'';'
    |   name='return' expression?';'
    ;

expression
        :   expression op=('++' | '--')                  # PostfixIncDec    // Precedence 1
        |   expression '(' parameterList? ')'            # FunctionCall
        |   expression '[' expression ']'                # IndexAccess
 //     |   expression '.' Identifier '('parameterList?')'# FunctionAccess
        |   expression '.' Identifier                    # MemberAccess

        |   <assoc=right> op=('++'|'--') expression      # UnaryExpr        // Precedence 2
        |   <assoc=right> op=('+' | '-') expression      # UnaryExpr
        |   <assoc=right> op=('!' | '~') expression      # UnaryExpr
        |   <assoc=right> 'new' creator                  # New

        |   expression op=('*' | '/' | '%') expression   # BinaryExpr       // Precedence 3
        |   expression op=('+' | '-') expression         # BinaryExpr       // Precedence 4
        |   expression op=('<<'|'>>') expression         # BinaryExpr       // Precedence 5
        |   expression op=('<' | '>') expression         # BinaryExpr       // Precedence 6
        |   expression op=('<='|'>=') expression         # BinaryExpr
        |   expression op=('=='|'!=') expression         # BinaryExpr       // Precedence 7
        |   expression op='&' expression                 # BinaryExpr       // Precedence 8
        |   expression op='^' expression                 # BinaryExpr       // Precedence 9
        |   expression op='|' expression                 # BinaryExpr       // Precedence 10
        |   expression op='&&' expression                # BinaryExpr       // Precedence 11
        |   expression op='||' expression                # BinaryExpr       // Precedence 12

        |   <assoc=right> expression op='=' expression   # BinaryExpr       // Precedence 14

        |   Identifier                                   # Identifier
        |   constant                                     # Literal
        |   '(' expression ')'                           # SubExpression
        |   'this'                                       # This
    ;

constant
    :   LogicalConstant  # Logic
    |   IntergerConstant # Int
    |   StringConstant   # Str
    |   NullConstant     # NULL
    ;

creator
    :   primitiveType (('['expression']') | (lop='['rop=']'))*
    ;

BOOL    :   'bool';
INT     :   'int';
STRING  :   'string';
//NULL    :   'null';
VOID    :   'void';
//TRUE    :   'true';
//FALSE   :   'false';
IF      :   'if';
FOR     :   'for';
WHILE   :   'while';
BREAK   :   'break';
CONTINUE:   'continue';
RETURN  :   'return';
NEW     :   'new';
CLASS   :   'class';
THIS    :   'this';

LParen : '(';
RParen : ')';
LBracket : '[';
RBracket : ']';
LBrace : '{';
RBrace : '}';

Less : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual : '>=';
LeftShift : '<<';
RightShift : '>>';

Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Div : '/';
Mod : '%';

And : '&';
Or : '|';
AndAnd : '&&';
OrOr : '||';
Caret : '^';
Not : '!';
Tilde : '~';

Question : '?';
Colon : ':';
Semi : ';';
Comma : ',';

Assign : '=';

Equal : '==';
NotEqual : '!=';

Dot : '.';

LogicalConstant
    :   'true'
    |   'false'
    ;

NullConstant
    :   'null'
    ;

IntergerConstant
    :   '0'
    |   [1-9][0-9]*
    ;

StringConstant
    :   '"' StringCharacters? '"'
    ;

Identifier
    :   [a-zA-Z]([a-zA-Z0-9_])*
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;

fragment
StringCharacter
    :   ~["\\]
    |   EscapeSequence
    ;

fragment
EscapeSequence
    :   '\\' [nr"'\\]
    ;

Whitespace
    :   [ \t]+ -> skip
    ;

Newline
    :   '\r'? '\n' -> skip
;

LineComment
    :   '//' ~[\r\n]* -> skip
;