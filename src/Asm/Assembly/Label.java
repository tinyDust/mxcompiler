package Asm.Assembly;

/**
 * Created by john on 17-5-17.
 */
public class Label extends Assembly {
    static int counter = 0;
    int id;
    public String name = null;
    public Label(){
        id = counter;
        name = "Label." + id;
        counter = counter + 1;
    }
    public Label(String _name){name = _name;}
    public void show(){
        System.out.print(name);
    }
}
