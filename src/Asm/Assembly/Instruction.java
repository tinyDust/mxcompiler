package Asm.Assembly;

/**
 * Created by john on 17-5-17.
 */
public abstract class Instruction extends Assembly {
    public abstract void show();
}
