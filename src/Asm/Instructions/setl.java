package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class setl extends Instruction {
    public Register reg;
    public setl(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tsetl\t");
        reg.show();
    }
}
