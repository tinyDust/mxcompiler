package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class not extends Instruction {
    public Register reg;
    public not(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tnot\t");
        reg.show();
    }
}
