package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.MemoryReference;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class lea extends Instruction {
    public Register reg;
    public MemoryReference mem;
    public lea(Register _reg, MemoryReference _mem){
        reg = _reg;
        mem = _mem;
    }
    public void show(){
        System.out.print("\tlea\t");
        reg.show();
        System.out.print(", ");
        mem.show();
    }
}
