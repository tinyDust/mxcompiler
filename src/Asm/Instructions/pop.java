package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class pop extends Instruction {
    public Register reg;
    public pop(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tpop\t");
        reg.show();
    }
}
