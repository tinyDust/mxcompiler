package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class setle extends Instruction {
    public Register reg;
    public setle(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tsetle\t");
        reg.show();
    }
}