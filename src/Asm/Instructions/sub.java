package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Operand;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class sub extends Instruction {
    public Register reg;
    public Operand val;
    public sub(Register _reg, Operand _val){
        reg = _reg;
        val = _val;
    }
    public void show(){
        System.out.print("\tsub\t");
        reg.show();
        System.out.print(", ");
        val.show();
    }
}