package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Operand;

/**
 * Created by john on 17-5-22.
 */
public class mov extends Instruction {
    public Operand op1, op2;
    public mov(Operand _op1, Operand _op2){
        op1 = _op1;
        op2 = _op2;
    }
    public void show(){
        System.out.print("\tmov\t");
        op1.show();
        System.out.print(", ");
        op2.show();
    }
}
