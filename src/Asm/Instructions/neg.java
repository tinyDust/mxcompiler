package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class neg extends Instruction {
    public Register reg;
    public neg(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tneg\t");
        reg.show();
    }
}
