package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class sete extends Instruction {
    public Register reg;
    public sete(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tsete\t");
        reg.show();
    }
}
