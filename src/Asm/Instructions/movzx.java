package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-24.
 */
public class movzx extends Instruction{
    public Register reg1, reg2;
    public movzx(Register _reg1, Register _reg2){
        reg1 = _reg1;
        reg2 = _reg2;
    }

    public void show(){
        System.out.print("\tmovzx\t");
        reg1.show();
        System.out.print(", ");
        reg2.show();
    }
}
