package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class push extends Instruction {
    public Register reg;
    public push(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tpush\t");
        reg.show();
    }
}
