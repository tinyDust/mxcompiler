package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Assembly.Label;
import Asm.Literal.Symbol;

/**
 * Created by john on 17-5-22.
 */
public class call extends Instruction {
    public Label label;
    public call(Label _label){
        label = _label;
    }
    public void show(){
        System.out.print("\tcall\t");
        label.show();
    }
}
