package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Assembly.Label;

/**
 * Created by john on 17-5-22.
 */
public class jnz extends Instruction {
    public Label label;
    public jnz(Label _label){
        label = _label;
    }
    public void show(){
        System.out.print("\tjnz\t");
        label.show();
    }
}
