package Asm.Instructions;

import Asm.Assembly.Instruction;
import Asm.Operand.Register;

/**
 * Created by john on 17-5-22.
 */
public class setge extends Instruction {
    public Register reg;
    public setge(Register _reg){
        reg = _reg;
    }
    public void show(){
        System.out.print("\tsetge\t");
        reg.show();
    }
}