package Asm.Literal;

/**
 * Created by john on 17-5-18.
 */
public abstract class Symbol extends Literal {
    public abstract void show();
}
