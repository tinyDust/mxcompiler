package Asm.Literal;

/**
 * Created by john on 17-5-18.
 */
public class IntegerLiteral extends Literal {
    public int value;
    public IntegerLiteral(int _value){ value = _value; }
    public void show(){System.out.print(value);}
}
