package Asm.Operand;

import static IRClass.Type.INT8;

/**
 * Created by john on 17-5-17.
 */
public class Register extends Operand {
    public enum RegisterClass{
        BX, R12, R13, R14, R15,
        R8, R9, R10, R11,
        AX, CX, DX, SI, DI, SP, BP,
    }
    public RegisterClass regc;
    public IRClass.Type type;

    public Register(RegisterClass _regc, IRClass.Type _type){
        regc = _regc;
        type = _type;
    }

    public void show(){
        switch (regc){
            case AX:
                if (type == INT8)
                    System.out.print("al");
                else
                    System.out.print("rax");
                break;
            case BP: System.out.print("rbp"); break;
            case BX: System.out.print("rbx"); break;
            case CX:
                if (type == INT8)
                    System.out.print("cl");
                else
                    System.out.print("rcx");
                break;
            case DI: System.out.print("rdi"); break;
            case DX: System.out.print("rdx"); break;
            case R8: System.out.print("r8") ; break;
            case R9: System.out.print("r9") ; break;
            case SI: System.out.print("rsi"); break;
            case SP: System.out.print("rsp"); break;
            case R10: System.out.print("r10"); break;
            case R11: System.out.print("r11"); break;
            case R12: System.out.print("r12"); break;
            case R13: System.out.print("r13"); break;
            case R14: System.out.print("r14"); break;
            case R15: System.out.print("r15"); break;
            default: System.out.print("no such register!"); break;
        }
    }
}
