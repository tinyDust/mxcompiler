package Asm.Operand;

/**
 * Created by john on 17-5-18.
 */
public class IndirectMemoryReference extends MemoryReference {
    public Register base;
    public int displacement;

    public IndirectMemoryReference(Register _base, int _displacement){
        base = _base;
        displacement = _displacement;
    }

    public void show(){
        System.out.print('[');
        base.show();
        if (displacement > 0){
            System.out.print('+');
            System.out.print(displacement);
        }
        else if (displacement < 0)
            System.out.print(displacement);
        System.out.print(']');
    }
}
