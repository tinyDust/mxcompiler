package Asm.Operand;

/**
 * Created by john on 17-5-17.
 */
public abstract class MemoryReference extends Operand {
    public abstract void show();
}
