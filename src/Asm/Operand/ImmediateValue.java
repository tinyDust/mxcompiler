package Asm.Operand;

import Asm.Literal.Literal;

/**
 * Created by john on 17-5-17.
 */
public class ImmediateValue extends Operand{
    public Literal literal;
    public ImmediateValue(Literal _literal){literal = _literal;}
    public void show(){literal.show();}
}
