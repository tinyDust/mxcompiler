package Asm.Operand;

import Asm.Assembly.Label;
import Asm.Literal.Symbol;

/**
 * Created by john on 17-5-18.
 */
public class DirectMemoryReference extends MemoryReference {
    public Label label;
    public Register reg;
    public DirectMemoryReference(Register _reg){
        reg = _reg;
    }
    public DirectMemoryReference(Label _label) {label = _label;}
    public void show(){
        if (reg != null) reg.show();
        else {
            System.out.print("[ rel ");
            label.show();
            System.out.print(" ]");
        }
    }
}
