package Asm.Operand;

import Asm.Assembly.Label;

/**
 * Created by john on 17-5-18.
 */
public class AbsoluteAddress extends Operand {
    Label label;
    public AbsoluteAddress(Label _label){
        label = _label;
    }
    public void show(){
        System.out.print(label.name);
    }
}
