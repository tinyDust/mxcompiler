package Asm;

import Asm.Assembly.Assembly;
import Asm.Instructions.*;
import Asm.Literal.Symbol;
import Asm.Operand.MemoryReference;
import Asm.Operand.Operand;
import Asm.Operand.Register;
import Asm.Assembly.Label;
import IRClass.Type;
import IRVisitor.VirtualStack;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static Asm.Operand.Register.RegisterClass.R10;
import static Asm.Operand.Register.RegisterClass.R11;
import static IRClass.Type.INT64;

/**
 * Created by john on 17-5-17.
 */
public class AssemblyCode {

    public List<Register> usedCalleeSaveRegister = new ArrayList<>();
    public List<Assembly> assembilities = new LinkedList<>();
    public VirtualStack virtualStack = new VirtualStack();

    private int hasSaved = 0;

    private void put(String s){
        System.out.println(s);
    }

    public void callerSave(){
        if (virtualStack.elementCount > 5){
            hasSaved++;
            push(new Register(R10, INT64));
            push(new Register(R11, INT64));
        }
    }

    public void recover(){
        if (hasSaved > 0){
            pop(new Register(R11, INT64));
            pop(new Register(R10, INT64));
            hasSaved--;
        }
    }

    public void show(){
        for (Assembly assembly : assembilities){
            assembly.show();
            if (assembly instanceof Label){
                put(":");
            }
            put("");
        }
    }

    public boolean used(Register register){
        return virtualStack.maxCount > register.regc.ordinal();
    }

    public void virtualPush(Register reg){
        virtualStack.addOne();
        mov(virtualStack.top(), reg);
    }

    public void virtualPop(Register reg){
        mov(reg, virtualStack.top());
        virtualStack.subOne();
    }

    public void addAll(AssemblyCode other){
        assembilities.addAll(other.assembilities);
    }

    // assembly code set
    // reg - reg, reg - mem, mem - reg
    public void mov(Operand op1, Operand op2){
        assembilities.add(new mov(op1, op2));
    }
    public void lea(Register reg, MemoryReference mem){assembilities.add(new lea(reg, mem));}
    public void push(Register reg){assembilities.add(new push(reg));}
    public void pop(Register reg){assembilities.add(new pop(reg));}
    public void add(Register reg, Operand val){assembilities.add(new add(reg, val));}
    public void sub(Register reg, Operand val){assembilities.add(new sub(reg, val));}
    public void mul(Register reg, Operand val){assembilities.add(new imul(reg, val));}
    public void idiv(Register reg){assembilities.add(new idiv(reg));}
    public void cdq(){assembilities.add(new cdq());}
    public void and(Register reg, Operand val){assembilities.add(new and(reg, val));}
    public void or(Register reg, Operand val){assembilities.add(new or(reg, val));}
    public void sal(Register reg, Operand val){assembilities.add(new sal(reg, val));}
    public void sar(Register reg, Operand val){assembilities.add(new sar(reg, val));}
    public void call(Label label){assembilities.add(new call(label));}
    public void ret(){assembilities.add(new ret());}
    public void jmp(Label label){assembilities.add(new jmp(label));}
    public void jnz(Label label){assembilities.add(new jnz(label));}
    public void neg(Register reg){assembilities.add(new neg(reg));}
    public void not(Register reg){assembilities.add(new not(reg));}
    public void test(Register reg1, Register reg2){assembilities.add(new test(reg1, reg2));}
    public void sete(Register reg){assembilities.add(new sete(reg));}
    public void setne(Register reg){assembilities.add(new setne(reg));}
    public void setl(Register reg){assembilities.add(new setl(reg));}
    public void setle(Register reg){assembilities.add(new setle(reg));}
    public void setg(Register reg){assembilities.add(new setg(reg));}
    public void setge(Register reg){assembilities.add(new setge(reg));}
    public void cmp(Register reg1, Register reg2){assembilities.add(new cmp(reg1, reg2));}
    public void xor(Register reg1, Operand val){assembilities.add(new xor(reg1, val));}
    public void movzx(Register reg1, Register reg2){assembilities.add(new movzx(reg1, reg2));}

    //pseudo assembly code set
    public void _file(String s){}
    public void _global(Symbol symbol){}
    public void _section(String s){}

    //Label
    public void label(Label label){
        assembilities.add(label);
    }
    public void label(Symbol symbol){}
}
