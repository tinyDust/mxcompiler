package IRVisitor;
import Asm.Assembly.Assembly;
import IRClass.*;
/**
 * Created by john on 17-5-21.
 */
public interface IRVisitor {
    void visit(Addr node);
    void visit(Assign node);
    void visit(Bin node);
    void visit(bool node);
    void visit(Call node);
    void visit(CJump node);
    void visit(DefinedVariable node);
    void visit(Expr node);
    void visit(ExprStmt node);
    void visit(Int node);
    void visit(IR node);
    void visit(Jump node);
    void visit(LabelStmt node);
    void visit(Mem node);
    void visit(Ref node);
    void visit(Return node);
    void visit(Stmt node);
    void visit(Str node);
    void visit(Uni node);
    void visit(Var node);
}
