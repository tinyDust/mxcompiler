package IRVisitor;

import AST.*;
import ASTVisitor.wrapper;
import Asm.Assembly.Assembly;
import Asm.Assembly.Label;
import Asm.AssemblyCode;
import Asm.Literal.IntegerLiteral;
import Asm.Literal.Symbol;
import Asm.Operand.DirectMemoryReference;
import Asm.Operand.ImmediateValue;
import Asm.Operand.IndirectMemoryReference;
import Asm.Operand.Register;
import IRClass.*;
import IRClass.Expr;
import IRClass.Stmt;
import TypeAndSymbol.Scope;
import TypeAndSymbol.VariableType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static AST.BinaryExpr.BinaryOp.MOD;
import static Asm.Operand.Register.RegisterClass.*;
import static IRClass.Type.*;

/**
 * Created by john on 17-5-18.
 */
public class CodeGenerator implements IRVisitor{

    public class StackFrameInfo{
        List<Register> saveRegs;
        int lvarSize;
        int tempSize;
        int saveRegsSize(){return saveRegs.size() * 8;}
        int lvarOffset(){return saveRegsSize();}
        int tempOffset(){return saveRegsSize() + lvarSize;}
        int frameSize() { return saveRegsSize() + lvarSize + tempSize; }
    }

    public AssemblyCode file;
    public LinkedList<AssemblyCode> funcParts = new LinkedList<>();

    private List<Register> calleeSaveRegisters = new LinkedList<>();
    private LinkedList<Str> globalStringList = new LinkedList<>();
    private LinkedList<String> globalVariables = new LinkedList<>();
    private AssemblyCode as;
    private Label epilogue;

    //stack info
    private Register.RegisterClass[] parameterRegister = new Register.RegisterClass[6];
    private int[] usedRegs = new int[16];

    private int getLen(String s){
        int length = s.length();
        int actualLen = 0;
        for (int i = 0; i < length; ++i){
            if (s.charAt(i) == '\\'){
                actualLen++;
                i++;
            }
            else{
                actualLen++;
            }
        }
        return actualLen;
    }

    public void show(){
        System.out.println("global main\n");

        System.out.println("section .data");
        for (String s : globalVariables){
            System.out.print("Global_" + s + ":\n\tdq\t0\n");
        }

        for (Str str: globalStringList){
            System.out.print("\tdq\t");
            int len = str.s.length();
            System.out.println(len);
            System.out.println(str.label.name + ':');
            System.out.print("\tdb\t");

            for (int i = 0; i < len; ++i){
                System.out.print(str.s.codePointAt(i));
                System.out.print(", ");
            }

            System.out.print("0");
            int align = (8 - (len + 1) % 8) % 8;
            for (int i = 0; i < align; ++i) System.out.print(", 0");
            System.out.println();
        }

        System.out.println("\nsection .text");
        for (AssemblyCode ac : funcParts) ac.show();
    }

    public CodeGenerator() {
        parameterRegister[0] = DI;
        parameterRegister[1] = SI;
        parameterRegister[2] = DX;
        parameterRegister[3] = CX;
        parameterRegister[4] = R8;
        parameterRegister[5] = R9;
//        calleeSaveRegisters.add(bp());
        calleeSaveRegisters.add(bx());
        calleeSaveRegisters.add(r12());
        calleeSaveRegisters.add(r13());
        calleeSaveRegisters.add(r14());
        calleeSaveRegisters.add(r15());
    }

    private void storeStringLiteral(LinkedList<Str> stringlist){
        globalStringList = stringlist;
    }

    public void generate(Program program){
        storeGlobalVariabls(program.varDecls);
        storeStringLiteral(program.GlobalStringList);

        file = new AssemblyCode();
        compileInitFunc(file, program.initFunc);
        funcParts.add(file);

        for (FuncDecl func: program.funcDecls){
            file = new AssemblyCode();
            compileFunc(file, func);
            funcParts.add(file);
        }

        for (ClassDecl classDecl: program.classDecls){
            for (FuncDecl func : classDecl.funcs){
                file = new AssemblyCode();
                compileClassFunc(file, func, classDecl.className);
                funcParts.add(file);
            }
            if (classDecl.cons != null){
                file = new AssemblyCode();
                compileCons(file, classDecl.cons, classDecl.className);
                funcParts.add(file);
            }
        }
    }

    private void storeGlobalVariabls(List<VarDecl> varDecls){
        for (VarDecl varDecl : varDecls){
            globalVariables.add(varDecl.name);
        }
    }

    private List<Register> usedCalleeSaveRegisters(AssemblyCode body) {
        List<Register> result = new ArrayList<>();
        for (Register reg : calleeSaveRegisters) {
            if (body.used(reg)) {
                result.add(reg);
            }
        }
        return result;
    }

    private void compileFunc(AssemblyCode file, FuncDecl func){
        init();
        file.assembilities.add(new Label(func.funcName));
        StackFrameInfo frame = new StackFrameInfo();
        frame.lvarSize = locateLocalVariables(func.scope);
        AssemblyCode body = compileStmts(func);
        frame.saveRegs = usedCalleeSaveRegisters(body);
        frame.tempSize = body.virtualStack.maxSize();
        fixOffsetLocalVariable(func.scope, frame.lvarOffset());
        fixTempVariableOffsets(body, frame.tempOffset());
        generateFunctionBody(file, body, frame, func.funcName);
    }

    private void fixTempVariableOffsets(AssemblyCode asm, int offset){
        asm.virtualStack.fixoffset(-offset);
    }

    private void compileInitFunc(AssemblyCode file, FuncDecl initFunc){
        init();
        file.assembilities.add(new Label(initFunc.funcName));
        StackFrameInfo frame = new StackFrameInfo();
        frame.lvarSize = locateInitFunctionLocalVariables(initFunc.scope);
        AssemblyCode body = compileStmts(initFunc);
        frame.saveRegs = usedCalleeSaveRegisters(body);
        frame.tempSize = body.virtualStack.maxSize();
        fixOffsetLocalVariableGlobal(initFunc.scope, frame.lvarOffset());
        fixTempVariableOffsets(body, frame.tempOffset());
        generateFunctionBody(file, body, frame, initFunc.funcName);
    }

    private void compileCons(AssemblyCode file, ConsDecl func, String className){
        init();
        StackFrameInfo frame = new StackFrameInfo();
        file.assembilities.add(new Label(className + func.funcName));
        frame.lvarSize = locateLocalVariables(func.scope);
        AssemblyCode body = compileStmts(func);
        frame.saveRegs = usedCalleeSaveRegisters(body);
        frame.tempSize = body.virtualStack.maxSize();
        fixOffsetLocalVariable(func.scope, frame.lvarOffset());
        fixTempVariableOffsets(body, frame.tempOffset());
        generateFunctionBody(file, body, frame, func.funcName);
    }

    private void compileClassFunc(AssemblyCode file, FuncDecl func, String className){
        init();
        file.assembilities.add(new Label(className + func.funcName));
        StackFrameInfo frame = new StackFrameInfo();
        frame.lvarSize = locateLocalVariables(func.scope);
        AssemblyCode body = compileStmts(func);
        frame.saveRegs = usedCalleeSaveRegisters(body);
        frame.tempSize = body.virtualStack.maxSize();
        fixOffsetLocalVariable(func.scope, frame.lvarOffset());
        fixTempVariableOffsets(body, frame.tempOffset());
        generateFunctionBody(file, body, frame, func.funcName);
    }

    private void generateFunctionBody(AssemblyCode file, AssemblyCode body, StackFrameInfo frame, String funcName){
        file.virtualStack.reset();
        prologue(file, frame.saveRegs, frame.frameSize());
        if (funcName.equals("main"))
            file.call(new Label("initGlobalVariables"));
        file.addAll(body);
        epilogue(file, frame.saveRegs, frame.frameSize());
        file.virtualStack.fixoffset(0);
    }

    private void prologue(AssemblyCode file, List<Register> usedRegs, int size){
        file.push(bp());
        file.mov(bp(), sp());
        for (Register reg: usedRegs){
            file.push(reg);
        }
        if (size % 16 != 0) size += 8;
        file.sub(sp(), new ImmediateValue(new IntegerLiteral(size - usedRegs.size() * 8)));
    }

    private void epilogue(AssemblyCode file, List<Register> usedRegs, int size){
        if (size % 16 != 0) size += 8;
        file.add(sp(), new ImmediateValue(new IntegerLiteral(size - usedRegs.size() * 8)));
        int s = usedRegs.size();
        for(int i = s - 1; i >= 0; --i){
            file.pop(usedRegs.get(i));
        }
        file.mov(sp(), bp());
        file.pop(bp());
        file.ret();
    }

    private int locateLocalVariables(Scope scope){
        return locateLocalVariables(scope, 0);
    }

    private void fixOffsetLocalVariable(Scope scope, int offset){
        for (Var var : scope.localVariables.values()){
            var.entity.memref.displacement -= offset;
        }

        for (Scope s : scope.children){
            fixOffsetLocalVariable(s, offset);
        }
    }

    private void fixOffsetLocalVariableGlobal(Scope scope, int offset){
        if (scope != null)
        for (Var var : scope.localVariables.values()){
            if (var.entity.name.contains("@"))
                var.entity.memref.displacement -= offset;
        }
    }

    private int locateInitFunctionLocalVariables(Scope scope){
        int len = 0;
        if (scope != null)
        for (Var var: scope.localVariables.values()){
            if (var.entity.name.contains("@")){
                len += 8;
                var.setMemRef(mem(bp(), -len));
            }
        }
        return len;
    }

    private int locateLocalVariables(Scope scope,int parentStacklen){
        int len = parentStacklen;
        for (Var var: scope.localVariables.values()){
            len += 8;
            var.setMemRef(mem(bp(), -len));
        }

        int maxLen = len;
        for (Scope s : scope.children){
            int childLen = locateLocalVariables(s, len);
            maxLen = Math.max(maxLen, childLen);
        }
        return maxLen;
    }

    private AssemblyCode compileStmts(FuncDecl func) {
        int count = 0;
        if (func.parameters != null)
        for (VarDecl varDecl : func.parameters){
            if (count < 6){
                as.mov(func.scope.localVariables.get(varDecl.id).entity.memref,
                        new Register(parameterRegister[count], INT64));
            }
            else{
                as.mov(ax(), mem(bp(), ((count - 6) + 2) * 8));
                as.mov(func.scope.localVariables.get(varDecl.id).entity.memref, ax());
            }
            count++;
        }

        for (IRClass.Stmt s : func.irs) {
            compileStmt(s);
        }
        as.label(epilogue);
        return as;
    }

    private AssemblyCode compileStmts(ConsDecl func) {
        as.mov(func.scope.localVariables.get(((VariableType)func.scope.get("this", null)).varDecl.id).entity.memref, di());
        for (IRClass.Stmt s : func.irs) {
            compileStmt(s);
        }
        as.label(epilogue);
        return as;
    }

    private void init(){
        as = new AssemblyCode();
        epilogue = new Label();
        for (int i = 0; i < 16; ++i){
            usedRegs[i] = 0;
        }
    }

    private void compileStmt(IRClass.Stmt s){ s.accept(this);}

    private void compileBinaryOp(BinaryExpr.BinaryOp op, Register lhs, Register rhs){
        switch(op){
            case ADD:
                as.add(lhs, rhs); break;
            case SUB:
                as.sub(lhs, rhs); break;
            case MUL:
                as.mul(lhs, rhs); break;
            case DIV: case MOD:
                as.cdq();
                as.idiv(rhs);
                if (op == MOD)
                    as.mov(lhs, dx());
                break;
            case BITWISE_AND:case LOGICAL_AND:
                as.and(lhs, rhs); break;
            case BITWISE_OR:case LOGICAL_OR:
                as.or(lhs, rhs); break;
            case SHL:
                as.sal(lhs, cx(INT8)); break;
            case SHR:
                as.sar(lhs, cx(INT8)); break;

            case EQ:
                as.cmp(lhs, rhs);
                as.sete(al());
                as.movzx(ax(), al());
                break;

            case NE:
                as.cmp(lhs, rhs);
                as.setne(al());
                as.movzx(ax(), al());
                break;

            case LT:
                as.cmp(lhs, rhs);
                as.setl(al());
                as.movzx(ax(), al());
                break;

            case LE:
                as.cmp(lhs, rhs);
                as.setle(al());
                as.movzx(ax(), al());
                break;

            case GT:
                as.cmp(lhs, rhs);
                as.setg(al());
                as.movzx(ax(), al());
                break;

            case GE:
                as.cmp(lhs, rhs);
                as.setge(al());
                as.movzx(ax(), al());
                break;

            case XOR:
                as.xor(lhs, rhs);
                break;
        }
    }

    //visitors-abstract
    public void visit(IR ir){ ir.accept(this);}
    public void visit(Expr expr){ expr.accept(this);}
    public void visit(Stmt stmt){ stmt.accept(this);}
    //visitors-real
    public void visit(Addr addr){
        if (addr.entity.addr != null) {
            as.mov(ax(), addr.entity.addr);
        }
        else
            as.lea(ax(), addr.entity.memref);
    }

    public void visit(Assign assign){
        if (assign.lhs instanceof Addr){
            assign.rhs.accept(this);
            if (((Addr) assign.lhs).entity.addr != null)
                as.mov(((Addr) assign.lhs).entity.addr, ax());
            else
                as.mov(((Addr) assign.lhs).entity.memref, ax());
        }
        else {
            assign.rhs.accept(this);
            as.virtualPush(ax());
            assign.lhs.accept(this);
            as.mov(cx(), ax());
            as.virtualPop(ax());
            as.mov(mem(cx()), ax());
        }
    }

    public void visit(Bin bin){
        BinaryExpr.BinaryOp op = bin.op;
        visit(bin.rhs);
        as.virtualPush(ax());
        visit(bin.lhs);
        as.virtualPop(cx());
        compileBinaryOp(op, ax(), cx());
    }

    public void visit(bool boo){
        int value = boo.value ? 1 : 0;
        as.mov(ax(), Imm(value));
    }

    public void visit(Call call){
  //      int sum = toAlign + pushPopDifference;
        int size = call.args.size();
        if (size < 6){
            for (int i = 0; i < size; ++i) {
                visit(call.args.get(i));
                as.virtualPush(ax());
            }

            for (int i = size - 1; i >= 0; --i){
                as.virtualPop(new Register(parameterRegister[i], INT64));
            }

            as.call(call.label);
        }
        else {
            for (int i = 0; i < 6; ++i){
                visit(call.args.get(i));
                as.virtualPush(ax());
            }
            int offset = 0;
            as.callerSave();
            if (((size - 6) * 8) % 16 != 0) as.sub(sp(), new ImmediateValue(new IntegerLiteral(offset = 8)));

            for(int i = size - 1; i > 5; --i){
                visit(call.args.get(i));
                as.push(ax());
            }

            for (int i = 5; i >= 0; --i){
                as.virtualPop(new Register(parameterRegister[i], INT64));
            }

            as.call(call.label);
            if (offset != 0)
                as.add(sp(), new ImmediateValue(new IntegerLiteral((size - 6) * 8 + offset)));
            as.recover();
        }

    }

    public void visit(CJump cjump){
        visit(cjump.condition);
        as.test(al(), al());
        as.jnz(cjump.thenlabel);
        as.jmp(cjump.elselabel);
    }

    public void visit(DefinedVariable dv){}
    public void visit(ExprStmt exprStmt){
        visit(exprStmt.expr);
    }
    public void visit(Int i){as.mov(ax() ,Imm(i.value));}
    public void visit(Jump jump){ as.jmp(jump.label);}

    public void visit(LabelStmt labelStmt){ as.label(labelStmt.label); }

    public void visit(Mem mem){
        mem.expr.accept(this);
        as.mov(ax(mem.type), mem(ax(mem.expr.type)));
    }

    public void visit(Ref ref){}

    public void visit(Return ret){
        if (ret.expr != null)
            ret.expr.accept(this);
        as.jmp(epilogue);
    }

    public void visit(Str str){as.mov(ax(), str.asmValue());}
    public void visit(Uni uni){
        Type type = uni.type;
        visit(uni.expr);
        switch(uni.op){
            case MINUS:
                as.neg(ax(type)); break;
            case SIM:
                as.not(ax(type)); break;
            case BANG:
                as.test(al(), al());
                as.sete(al());
                break;
            default:
                throw new Error("CodeGen Uni Error");
        }
    }

    public void visit(Var var){
        if (var.entity.memref != null)
            as.mov(ax(var.type), var.entity.memref);
        else
            as.mov(ax(var.type), var.entity.addr);
    }

    // Registers
    private Register ax(){ usedRegs[AX.ordinal()]++;return new Register(AX, INT64); }
    private Register bx(){ usedRegs[BX.ordinal()]++;return new Register(BX, INT64);}
    private Register cx(){ usedRegs[CX.ordinal()]++;return new Register(CX, INT64);}
    private Register dx(){ usedRegs[DX.ordinal()]++;return new Register(DX, INT64);}
    private Register si(){ usedRegs[SI.ordinal()]++;return new Register(SI, INT64);}
    private Register di(){ usedRegs[DI.ordinal()]++;return new Register(DI, INT64);}
    private Register sp(){ usedRegs[SP.ordinal()]++;return new Register(SP, INT64);}
    private Register bp(){ usedRegs[BP.ordinal()]++;return new Register(BP, INT64);}
    private Register ax(IRClass.Type type) { usedRegs[AX.ordinal()]++;return new Register(AX, type);}
    private Register bx(IRClass.Type type) { usedRegs[BX.ordinal()]++;return new Register(BX, type);}
    private Register cx(IRClass.Type type) { usedRegs[CX.ordinal()]++; return new Register(CX, type);}
    private Register dx(IRClass.Type type) { usedRegs[DX.ordinal()]++; return new Register(DX, type);}
    private Register r12(){ usedRegs[R12.ordinal()]++;return new Register(R12, INT64);}
    private Register r13() {usedRegs[R13.ordinal()]++;return new Register(R13, INT64);}
    private Register r14() {usedRegs[R14.ordinal()]++;return new Register(R14, INT64);}
    private Register r15() {usedRegs[R15.ordinal()]++;return new Register(R15, INT64);}
    private Register al() { usedRegs[AX.ordinal()]++;return ax(INT8); }


    //ImmediateValue and MemRef
    private ImmediateValue Imm(int value){ return new ImmediateValue(new IntegerLiteral(value));}
    private ImmediateValue Imm(Symbol symbol){return new ImmediateValue(symbol);}

    private IndirectMemoryReference mem(Register reg){
        return new IndirectMemoryReference(reg, 0);
    }

    public IndirectMemoryReference mem(Register base, int displacement){
        return new IndirectMemoryReference(base, displacement);
    }
}