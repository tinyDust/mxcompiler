package IRVisitor;

import Asm.AssemblyCode;
import Asm.Operand.IndirectMemoryReference;
import Asm.Operand.Operand;
import Asm.Operand.Register;

import java.util.ArrayList;
import java.util.List;

import static Asm.Operand.Register.RegisterClass.*;
import static IRClass.Type.INT64;

/**
 * Created by john on 17-5-29.
 */
public class VirtualStack {
    public int elementCount;
    public int maxCount = 0;
    public int offset;
    private int max;
    private Register[] registers = new Register[7];
    private List<IndirectMemoryReference> memrefs =
            new ArrayList<>();
    private int difference = 0;
    public VirtualStack(){
        reset();
        registers[0] = new Register(BX, INT64);
        registers[1] = new Register(R12, INT64);
        registers[2] = new Register(R13, INT64);
        registers[3] = new Register(R14, INT64);
        registers[4] = new Register(R15, INT64);
        registers[5] = new Register(R10, INT64);
        registers[6] = new Register(R11, INT64);
    }

    public void addOne(){
        if (elementCount >= 7) extend(8);
        elementCount++;
        maxCount = Math.max(maxCount, elementCount);
    }

    public void subOne(){
        if (elementCount > 7) rewind(8);
        elementCount--;
    }

    public void extend(int n){
        offset += n;
        max = Math.max(offset, max);
    }

    public void rewind(int n){
        offset -= n;
    }

    public Operand top(){
        if (elementCount <= 7) return registers[elementCount - 1];
        else{
            IndirectMemoryReference mem =  new IndirectMemoryReference(new Register(BP, INT64),  -offset);
            memrefs.add(mem);
            return mem;
        }
    }

    public void reset(){
        elementCount = 0;
        offset = 0;
        max = 0;
        memrefs.clear();
    }

    public int maxSize(){
        return max;
    }

    public void fixoffset(int n){
        assert n <= 0;
        for (IndirectMemoryReference mem : memrefs){
            mem.displacement += n;
        }
    }
}
