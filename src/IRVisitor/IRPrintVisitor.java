package IRVisitor;

import AST.ASTnode;
import AST.BinaryExpr;
import AST.ClassDecl;
import AST.FuncDecl;
import Asm.Assembly.Label;
import IRClass.*;

/**
 * Created by john on 17-5-21.
 */
public class IRPrintVisitor implements IRVisitor {

    public void start(AST.Program program){
        for (IR ir : program.initFunc.irs){
            visit(ir);
        }
        for (FuncDecl funcDecl: program.funcDecls){
            for (IR ir : funcDecl.irs){
                visit(ir);
            }
        }

        System.out.println("Classes:");
        System.out.println("-----------------------");
        System.out.println("-----------------------");
        for (ClassDecl classDecl : program.classDecls){
            for (FuncDecl funcDecl : classDecl.funcs){
                System.out.println("ClassFunc:");
                System.out.println("-----------------------");
                for (IR ir : funcDecl.irs){
                    visit(ir);
                }
            }
        }
    }

    private int depth = 0;
    private void putTab(){
        for (int i = 0; i < depth; ++i){
            System.out.print('\t');
        }
    }

    private void putl(String s){
        putTab();
        System.out.println(s);
    }

    private void putl(Integer i){
        putTab();
        System.out.println(i);
    }

    private void putl(BinaryExpr.BinaryOp op){
        switch(op){
            case GE: putl("OP:>="); break;
            case LE: putl("OP:<="); break;
            case NE: putl("OP:!="); break;
            case LT: putl("OP:<" ); break;
            case EQ: putl("OP:=="); break;
            case MOD: putl("OP:%"); break;
            case SHL: putl("OP:<<"); break;
            case BITWISE_AND: putl("OP:&"); break;
            case DIV: putl("OP:/"); break;
            case MUL: putl("OP:*"); break;
            case SUB: putl("OP:-"); break;
            case ADD: putl("OP:+"); break;
            case LOGICAL_AND: putl("OP:&&"); break;
            case LOGICAL_OR: putl("OP:||"); break;
            case ASSIGN: putl("OP:="); break;
            case GT: putl("OP:>"); break;
            case BITWISE_OR: putl("OP:|"); break;
            case XOR: putl("OP:^"); break;
            case SHR: putl("OP:>>"); break;
            default: break;
        }
    }

    private void put(String s){
        putTab();
        System.out.print(s);
    }

    private void tab(){
        System.out.print('\t');
    }

    public void visit(Addr node){
        putl("<Addr>");
        putl(node.entity.name);
    }

    public void visit(Assign node){
        putl("<Assign>");
        depth++;
        visit(node.lhs);
        visit(node.rhs);
        depth--;
    }

    public void visit(Bin node){
        putl("<Bin>");
        putl(node.op);
        depth++;
        visit(node.lhs);
        visit(node.rhs);
        depth--;
    }

    public void visit(bool node){
        putl("<Bool>");
    }

    public void visit(Call node){
        putl("<Call>");
        putl(node.label.name);
        depth++;
        for (Expr expr:node.args){
            putl("parameter:");
            visit(expr);
        }
        depth--;
    }

    public void visit(CJump node){
        put("<CJump>");
        tab();
        node.thenlabel.show();
        tab();
        node.elselabel.show();
        putl("");
        depth++;
        putl("condition");
        visit(node.condition);
        depth--;
    }

    public void visit(DefinedVariable node){
        putl("<DefinedVariable>");
    }

    public void visit(Expr node){
        if (node != null) node.accept(this);
    }

    public void visit(ExprStmt node){
        putl("<ExprStmt>");
        depth++;
        visit(node.expr);
        depth--;
    }

    public void visit(Int node){
        putl("<Int>");
        putl(node.value);
    }
    public void visit(IR node){node.accept(this);}
    public void visit(Jump node){
        putl("<Jump>");
        tab();
        visit(node.label);
        putl("");
    }

    public void visit(LabelStmt node){
        put("<Label>");
        visit(node.label);
        putl(":");
    }

    public void visit(Label label){
        label.show();
    }

    public void visit(Mem node){
        putl("<Mem>");
        depth++;
        visit(node.expr);
        depth--;
    }
    public void visit(Ref node){
        putl("<Ref>");
        depth++;
        visit(node.tmp);
        depth--;
    }

    public void visit(Return node){
        putl("<Return>");
        depth++;
        if (node.expr != null) visit(node.expr);
        depth--;
    }

    public void visit(Stmt node){node.accept(this);}
    public void visit(Str node){
        putl("<Str>");
        putl(node.s);
        putl("\n");
    }
    
    public void visit(Uni node){
        putl("<Uni>");
        depth++;
        visit(node.expr);
        depth--;
    }
    
    public void visit(Var node){
        putl("<Var>");
        putl(node.entity.name);
    }
}
