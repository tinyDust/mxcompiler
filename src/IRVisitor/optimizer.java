package IRVisitor;

import AST.Decl;
import AST.FuncDecl;
import AST.Program;
import IRClass.*;

import java.util.HashMap;
import java.util.Map;

import static AST.BinaryExpr.BinaryOp.ADD;
import static AST.BinaryExpr.BinaryOp.MUL;
import static AST.BinaryExpr.BinaryOp.SUB;

/**
 * Created by john on 17-5-30.
 */
public class optimizer {
    public Program program;

    public optimizer(Program _program){
        program = _program;
    }

    public void BinaryOptimize(){
        for (Decl decl : program.allfuncs.values()){
            if (decl instanceof FuncDecl){
                for (Stmt stmt : ((FuncDecl) decl).irs){
                    if (stmt instanceof Assign){
                        HashMap<Var, Integer> map = eliminate(((Assign) stmt).rhs);
                        IRClass.Expr currentExpr = null;
                        if (map == null) continue;
                        else{
                            for (Map.Entry<Var, Integer> entry : map.entrySet()){
                                if (currentExpr == null){
                                    currentExpr = getBin(entry);
                                }
                                else {
                                    currentExpr = new Bin(ADD, currentExpr, getBin(entry));
                                }
                            }
                        }
                        ((Assign) stmt).rhs = currentExpr;
                    }
                }
            }
        }
    }

    public IRClass.Expr getBin(Map.Entry<Var, Integer> entry){
        if (entry.getValue() == 1){
            return entry.getKey();
        }
        else{
            return new Bin(MUL, entry.getKey(), new Int(entry.getValue()));
        }
    }

    public HashMap<Var, Integer> eliminate(IRClass.Expr expr){
        HashMap<Var, Integer> returnMap = new HashMap<>();
        if (expr instanceof Var){
            returnMap.put((Var)expr, 1);
            return returnMap;
        }
        else if (expr instanceof Bin){
            if (((Bin) expr).op == ADD || ((Bin)expr).op == SUB){
                HashMap<Var, Integer> m1 = eliminate(((Bin) expr).lhs);
                HashMap<Var, Integer> m2 = eliminate(((Bin) expr).rhs);
                if (m1 == null || m2 == null) return null;
                else {
                    int transformer = ((Bin) expr).op == ADD ? 1 : -1;
                    returnMap = m1;
                    for (Var var : m2.keySet()){
                        if (returnMap.containsKey(var)){
                            returnMap.put(var, returnMap.get(var) + transformer * m2.get(var));
                        }
                        else{
                            returnMap.put(var, transformer * m2.get(var));
                        }
                    }
                    return returnMap;
                }
            }
        }
        return null;
    }

}
