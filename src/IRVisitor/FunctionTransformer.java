package IRVisitor;

import IRClass.*;

import java.util.HashMap;

/**
 * Created by john on 17-5-30.
 */
public class FunctionTransformer implements IRVisitor {
    
    private HashMap<String, IRClass.Expr> varExprs;
    private IRClass.Expr result = null;

    public IRClass.Expr result(){
        return result;
    }

    public FunctionTransformer(HashMap<String, IRClass.Expr> _varExprs){
        varExprs = _varExprs;
    }
    
    private void error(){
        System.out.println("Error in inline");
        System.exit(1);
    }
    
    public void visit(Addr node){
        error();
    }
    
    public void visit(Assign node){
        error();
    }

    private IRClass.Expr test(IRClass.Expr expr){
        if (expr instanceof Var){
            return varExprs.get(((Var) expr).entity.name);
        }
        else{
            visit(expr);
            return expr;
        }
    }

    public void visit(Bin node){
        node.lhs = test(node.lhs);
        node.rhs = test(node.rhs);
    }
    
    public void visit(bool node){return;}
    public void visit(Call node){
        node.args.forEach(x -> x = test(x));
    }

    public void visit(CJump node){error();}
    public void visit(DefinedVariable node){error();};
    public void visit(Expr node){
        if (result == null) result = node;
        if (node != null) node.accept(this);
    }

    public void visit(ExprStmt node){error();}
    public void visit(Int node){}
    public void visit(IR node){if (node != null) node.accept(this);}
    public void visit(Jump node){error();}
    public void visit(LabelStmt node){error();}
    public void visit(Mem node){node.expr = test(node.expr);}
    public void visit(Ref node){error();}
    public void visit(Return node){error();}
    public void visit(Stmt node){error();}
    public void visit(Str node){}
    public void visit(Uni node){node.expr = test(node.expr);}
    public void visit(Var node){error();}
}
