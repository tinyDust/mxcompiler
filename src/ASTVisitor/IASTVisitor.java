package ASTVisitor;

import AST.*;

/**
 * Created by fzy on 17-3-31.
 */
public interface IASTVisitor<T> {
    public T visit(ASTnode node);
    public T visit(Decl node);
    public T visit(Stmt node);
    public T visit(Expr node);

    public T visit(Program node);

    public T visit(ClassDecl node);
    public T visit(ConsDecl node);
    public T visit(FuncDecl node);
    public T visit(MemberDecl node);
    public T visit(VarDecl node);

    public T visit(BlockStmt node);
    public T visit(EmptyStmt node);
    public T visit(ForStmt node);
    public T visit(IfStmt node);
    public T visit(JumpStmt node);
    public T visit(VarDeclStmt node);
    public T visit(WhileStmt node);

    public T visit(BinaryExpr node);
    public T visit(BoolExpr node);
    public T visit(FunctionCallExpr node);
    public T visit(IdentifierExpr node);
    public T visit(IndexAccessExpr node);
    public T visit(IntExpr node);
    public T visit(LogicalExpr node);
    public T visit(MemberAccess node);
    public T visit(NewExpr node);
    public T visit(NullExpr node);
    public T visit(PostUnaryExpr node);
    public T visit(StringExpr node);
    public T visit(ThisExpr node);
    public T visit(UnaryExpr node);
}
