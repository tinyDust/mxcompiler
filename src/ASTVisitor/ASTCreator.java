package ASTVisitor;

import AST.*;
import LexerParser.MxBaseListener;
import LexerParser.MxParser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by fzy on 17-4-1.
 */
public class ASTCreator extends MxBaseListener {
    public Program program = new Program();
    public ParseTreeProperty<ASTnode> ASTnodes = new ParseTreeProperty<>();

    private void error(String s){
        System.out.println(s);
    }

    private boolean checkCreator(String creator){
        creator.replace(" ", "");
        int ExpressionRight = -1 , TokenRight = -1;
        for (int i = creator.length() - 1; i >= 0; --i)
            if (creator.charAt(i) == ']' && creator.charAt(i - 1) != '[') {
                ExpressionRight = i;
                break;
            }
        for (int i = creator.length() - 1; i >= 0; --i)
            if (creator.charAt(i) == ']' && creator.charAt(i - 1) == '[') {
                TokenRight = i;
                break;
            }
        return  ExpressionRight == - 1 || TokenRight == -1 || TokenRight > ExpressionRight;
    }

    // program : programSection* EOF
    @Override public void exitProgram(MxParser.ProgramContext ctx) {
        for (Object x : ctx.programSection()){
            if (x instanceof MxParser.FuncDeclContext) {
                program.funcDecls.add((FuncDecl) ASTnodes.get((MxParser.FuncDeclContext)x));
            }
            else if (x instanceof MxParser.VarDeclContext) {
                program.varDecls.add((VarDecl) ASTnodes.get((MxParser.VarDeclContext)x));
            }
            else
                program.classDecls.add((ClassDecl) ASTnodes.get((MxParser.ClassDeclContext)x));

            program.Decls.add((Decl) ASTnodes.get((ParseTree)x));
        }
        ASTnodes.put(ctx, program);
    }

    // programsection : functionDeclaration
    @Override public void exitFuncDecl(MxParser.FuncDeclContext ctx){
        ASTnodes.put(ctx, ASTnodes.get(ctx.functionDeclaration()));
    }

    // programSection : classDeclaration
    @Override public void exitClassDecl(MxParser.ClassDeclContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.classDeclaration()));
    }

    // programSection : variableDeclaration
    @Override public void exitVarDecl(MxParser.VarDeclContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.variableDeclaration()));
    }

    // classDeclaration
    @Override public void exitClassDeclaration(MxParser.ClassDeclarationContext ctx) {
        LinkedList<VarDecl> vars = new LinkedList<>();
        LinkedList<FuncDecl> funcs = new LinkedList<>();
        ConsDecl cons = null;
        for ( LexerParser.MxParser.MemberDeclarationContext x : ctx.memberDeclaration()){
            ASTnode tem = ASTnodes.get(x);
            if (tem instanceof VarDecl) vars.add((VarDecl) tem);
            else if (tem instanceof FuncDecl) funcs.add((FuncDecl) tem);
            else if (tem instanceof ConsDecl) cons = (ConsDecl) tem;
            else error("Error In ClassDecl In ASTCreator");
        }
        ASTnodes.put(ctx, new ClassDecl(ctx.Identifier().getText(), vars, funcs, cons));
    }

    //memberDeclarations : functionDeclaration
    @Override public void exitMemFunc(MxParser.MemFuncContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.functionDeclaration()));
    }

    //memberDeclarations : constructorDeclaration
    @Override public void exitMemCons(MxParser.MemConsContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.constructorDeclaration()));
    }

    //memberDeclarations : variableDeclaration
    @Override public void exitMemVar(MxParser.MemVarContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.variableDeclaration()));
    }

    @Override public void exitFunctionDeclaration(MxParser.FunctionDeclarationContext ctx) {
        TemType temType = ctx.type() == null ? new TemType("void", 0)
                : new TemType(ctx.type().primitiveType().getText(), ctx.type().getText());
        LinkedList<VarDecl> varDecls = new LinkedList<>();
        if (ctx.typeParameterList() != null)
            ctx.typeParameterList().variableDeclaration().forEach(x -> varDecls.add((VarDecl)ASTnodes.get(x)));
        ASTnodes.put(ctx, new FuncDecl(temType, ctx.Identifier().getText(), varDecls, (BlockStmt) ASTnodes.get(ctx.blockStatement())));
    }

    @Override public void exitConstructorDeclaration(MxParser.ConstructorDeclarationContext ctx) {
        LinkedList<VarDecl> varDecls = new LinkedList<>();
        if (ctx.typeParameterList() != null)
            ctx.typeParameterList().variableDeclaration().forEach(x -> varDecls.add((VarDecl)ASTnodes.get(x)));
        ASTnodes.put(ctx, new ConsDecl(ctx.Identifier().getText(), varDecls, (BlockStmt)ASTnodes.get(ctx.blockStatement())));
    }

    @Override public void exitType(MxParser.TypeContext ctx) { }

    @Override public void exitPrimitiveType(MxParser.PrimitiveTypeContext ctx) { }

    @Override public void exitParameterList(MxParser.ParameterListContext ctx) { }

    @Override public void exitTypeParameterList(MxParser.TypeParameterListContext ctx) { }

    @Override public void exitBlockStatement(MxParser.BlockStatementContext ctx) {
        List<Stmt> stmts = new ArrayList<>();
        if (ctx.statement() != null)
            ctx.statement().forEach(x -> stmts.add((Stmt)ASTnodes.get(x)));
        ASTnodes.put(ctx, new BlockStmt(stmts));
    }

    // variableDeclaration
    @Override public void exitVariableDeclaration(MxParser.VariableDeclarationContext ctx) {
        TemType type = new TemType(ctx.type().primitiveType().getText(), ctx.type().getText());
        ASTnodes.put(ctx, new VarDecl(type, ctx.Identifier().getText(), (Expr)ASTnodes.get(ctx.expression())));
    }

    @Override public void exitExprStmt(MxParser.ExprStmtContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.expression()));
    }

    @Override public void exitVarDeclStmt(MxParser.VarDeclStmtContext ctx) {
        ASTnodes.put(ctx, new VarDeclStmt((VarDecl) ASTnodes.get(ctx.variableDeclarationStmt().variableDeclaration())));
    }

    @Override public void exitConditionStmt(MxParser.ConditionStmtContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.conditionStatement()));
    }

    @Override public void exitLoopStmt(MxParser.LoopStmtContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.loopStatement()));
    }

    @Override public void exitJumpStmt(MxParser.JumpStmtContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.jumpStatement()));
    }

    @Override public void exitBlockStmt(MxParser.BlockStmtContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.blockStatement()));
    }

    @Override public void exitEmptyStmt(MxParser.EmptyStmtContext ctx) {
        ASTnodes.put(ctx, new EmptyStmt());
    }

    @Override public void exitConditionStatement(MxParser.ConditionStatementContext ctx) {
        ASTnodes.put(ctx, new IfStmt((Expr) ASTnodes.get(ctx.expression()),
                (Stmt) ASTnodes.get(ctx.statement(0)), (Stmt) ASTnodes.get(ctx.statement(1))));
    }

    @Override public void exitWhile(MxParser.WhileContext ctx) {
        ASTnodes.put(ctx, new WhileStmt((Expr)ASTnodes.get(ctx.expression()), (Stmt)ASTnodes.get(ctx.statement())));
    }

    @Override public void exitFor(MxParser.ForContext ctx) {
        ASTnodes.put(ctx, new ForStmt((Expr) ASTnodes.get(ctx.init), (Expr) ASTnodes.get(ctx.cond)
        , (Expr) ASTnodes.get(ctx.step), (Stmt) ASTnodes.get(ctx.statement())));
    }

    @Override public void exitJumpStatement(MxParser.JumpStatementContext ctx) {
        ASTnodes.put(ctx, new JumpStmt(ctx.name.getText(), (Expr) ASTnodes.get(ctx.expression())));
    }

    @Override public void exitNew(MxParser.NewContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.creator()));
    }

    @Override public void exitIdentifier(MxParser.IdentifierContext ctx) {
        ASTnodes.put(ctx, new IdentifierExpr(ctx.Identifier().getText()));
    }

    @Override public void exitMemberAccess(MxParser.MemberAccessContext ctx) {
        ASTnodes.put(ctx, new MemberAccess((Expr)ASTnodes.get(ctx.expression()), ctx.Identifier().getText()));
    }

    @Override public void exitIndexAccess(MxParser.IndexAccessContext ctx) {
        ASTnodes.put(ctx, new IndexAccessExpr((Expr)ASTnodes.get(ctx.expression(0)), (Expr)ASTnodes.get(ctx.expression(1))));
    }

    @Override public void exitLiteral(MxParser.LiteralContext ctx) {
        ASTnodes.put(ctx, ASTnodes.get(ctx.constant()));
    }

    @Override public void exitBinaryExpr(MxParser.BinaryExprContext ctx) {
        ASTnodes.put(ctx, new BinaryExpr((Expr)ASTnodes.get(ctx.expression(0))
                , (Expr)ASTnodes.get(ctx.expression(1)), ctx.op.getText()));
    }

    @Override public void exitThis(MxParser.ThisContext ctx) {
        ASTnodes.put(ctx, new ThisExpr());
    }

    @Override public void exitFunctionCall(MxParser.FunctionCallContext ctx) {
        LinkedList<Expr> parameters = new LinkedList<>();
        if (ctx.parameterList() != null)
            ctx.parameterList().expression().forEach(x -> parameters.add((Expr)ASTnodes.get(x)));
        ASTnodes.put(ctx, new FunctionCallExpr((Expr)ASTnodes.get(ctx.expression()), parameters));
    }

    @Override public void exitPostfixIncDec(MxParser.PostfixIncDecContext ctx) {
        ASTnodes.put(ctx, new PostUnaryExpr(ctx.op.getText(), (Expr)ASTnodes.get(ctx.expression())));
    }

    @Override public void exitUnaryExpr(MxParser.UnaryExprContext ctx) {
        ASTnodes.put(ctx, new UnaryExpr(ctx.op.getText(), (Expr)ASTnodes.get(ctx.expression())));
    }

    @Override public void exitSubExpression(MxParser.SubExpressionContext ctx) {
        ASTnodes.put(ctx, (Expr)ASTnodes.get(ctx.expression()));
    }

    @Override public void exitLogic(MxParser.LogicContext ctx) {
        ASTnodes.put(ctx, new BoolExpr(ctx.LogicalConstant().getText().equals("true")));
    }

    @Override public void exitInt(MxParser.IntContext ctx) {
        ASTnodes.put(ctx, new IntExpr(Integer.parseInt(ctx.IntergerConstant().getText())));
    }

    @Override public void exitStr(MxParser.StrContext ctx) {
        String s = ctx.StringConstant().getText();
        s = s.replace("\\n", "\n");
        s = s.replace("\\t", "\t");
        s = s.replace("\\\\", "\\");
        s = s.replace("\\\"", "\"");
        ASTnodes.put(ctx, new StringExpr(s));
    }

    @Override public void exitNULL(MxParser.NULLContext ctx) {
        ASTnodes.put(ctx, new NullExpr());
    }

    @Override public void exitCreator(MxParser.CreatorContext ctx) {
        if (!checkCreator(ctx.getText())) System.exit(2);
        List<Expr> exprs = new ArrayList<>();
        if (ctx.expression() != null)
            ctx.expression().forEach(x -> exprs.add((Expr)ASTnodes.get(x)));
        ASTnodes.put(ctx, new NewExpr(new TemType(ctx.primitiveType().getText(), ctx.getText()), exprs));
    }

    @Override public void exitEveryRule(ParserRuleContext ctx) { }

    @Override public void visitTerminal(TerminalNode node) { }

    @Override public void visitErrorNode(ErrorNode node) { }
}