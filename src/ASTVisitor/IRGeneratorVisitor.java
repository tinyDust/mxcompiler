package ASTVisitor;

import AST.*;
import AST.Expr;
import Asm.Assembly.Label;
import Asm.Operand.DirectMemoryReference;
import IRClass.*;
import IRClass.Stmt;
import IRVisitor.FunctionTransformer;
import TypeAndSymbol.Scope;
import AST.BinaryExpr.BinaryOp;
import TypeAndSymbol.VariableType;
import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static IRClass.Type.*;

/**
 * Created by john on 17-5-9.
 */
public class IRGeneratorVisitor implements IASTVisitor<IRClass.Expr>{
    private LinkedList<IRClass.Stmt> stmts;
    private LinkedList<Scope> scopeStack;
    private LinkedList<Label> breakStack;
    private LinkedList<Label> continueStack;
    private LinkedList<Str> GlobalStringList;
    private int exprLevel = 0;
    private boolean isClass = false;
    private boolean INLINE = true;
    private int inlineLevel = 0;
    private ResolverVisitor res;
    private HashMap<String, Decl> allfuncs;

    public IRGeneratorVisitor(ResolverVisitor _res){
        res = _res;
    }

    public void generate(Program program){
        allfuncs = program.allfuncs;
        GlobalStringList = program.GlobalStringList;
        createrInitFunc(program.initFunc, program.varDecls);
//        initFunc = program.initFunc;
//        initFunc.scope = new Scope(null, environment.WHOLE);
//        program.varDecls.forEach(this::translateVar);
        program.classDecls.forEach(this::translateClass);
        program.funcDecls.forEach(this::translateFunc);
    }

    private void error(String s){
        System.out.println(s);
    }

    private void translateFunc(FuncDecl decl){
        init();
        decl.scope.initLocal();
        if (isClass){
            VarDecl varDecl = new VarDecl(null, "this", null);
            decl.scope.put("this", new VariableType(null, varDecl));
            decl.parameters.push(varDecl);
            Var tmp = new Var(INT64, new Entity("this"));
            decl.scope.localVariables.put(varDecl.id, tmp);
//            decl.parametersIR.push(tmp);
        }
        transformStmt(decl.blockStmt);
        decl.setIR(stmts);
    }

    private void translateCons(ConsDecl cons){
        init();
        cons.scope.initLocal();

        VarDecl varDecl = new VarDecl(null, "this", null);
        cons.scope.put("this", new VariableType(null, varDecl));
        cons.parameters.push(varDecl);
        Var tmp = new Var(INT64, new Entity("this"));
        cons.scope.localVariables.put(varDecl.id, tmp);

        transformStmt(cons.blockStmt);
        cons.setIR(stmts);
    }

    private void createrInitFunc(FuncDecl func, LinkedList<VarDecl> varDecls){
        stmts = new LinkedList<>();
        for (VarDecl varDecl : varDecls){
            func.scope = varDecl.scope;
            Var temVar = allocateVar(func.scope, varDecl.id, varDecl.name);
            temVar.setMemRef(new DirectMemoryReference(new Label("Global_"+varDecl.name)));
            func.scope.localVariables.put(varDecl.id, temVar);
            visit(varDecl);
        }
        func.irs = stmts;
    }
/*
    private void translateVar(VarDecl var){
        stmts = new LinkedList<>();
        if (var.expr != null){
            IRClass.Expr expr = transformExpr(var.expr);
            assign(var.scope.localVariables.get(var.name), expr);
        }
        initFunc.irs.addAll(stmts);
    }
*/
    private void translateClass(ClassDecl decl){
        isClass = true;
        decl.funcs.forEach(this::translateFunc);
        if (decl.cons != null)
            translateCons(decl.cons);
        isClass = false;
    }

    private void init(){
        stmts = new LinkedList<>();
        scopeStack = new LinkedList<>();
        breakStack = new LinkedList<>();
        continueStack = new LinkedList<>();
    }

    private boolean isStmt(){return exprLevel == 0;}

    private void transformStmt(AST.Stmt stmt){
        if (stmt == null) return;
        stmt.accept(this);
    }

    private void transformStmt(AST.Expr expr){
        if (expr == null) return;
        expr.accept(this);
    }

    private IRClass.Expr transformExpr(AST.Expr expr){
        if (expr == null) return null;
        ++exprLevel;
        IRClass.Expr _expr = (IRClass.Expr) expr.accept(this);
        --exprLevel;
        return _expr;
    }
    
    public IRClass.Expr visit(ASTnode node){if (node != null) node.accept(this); return null;}
    public IRClass.Expr visit(Decl node){if (node != null) node.accept(this); return null;}
    public IRClass.Expr visit(AST.Stmt node){if (node != null) node.accept(this); return null;}
    public IRClass.Expr visit(AST.Expr node){if (node != null) node.accept(this); return null;}

    public IRClass.Expr visit(Program node){error("unable to approach Program"); return null;}

    public IRClass.Expr visit(ClassDecl node){error("haven't dealt with classdecl"); return null;}
    public IRClass.Expr visit(ConsDecl node){error("haven't dealt with consdecl"); return null;}
    public IRClass.Expr visit(FuncDecl node){error("unable to approach FuncDecl"); return null;}
    public IRClass.Expr visit(MemberDecl node){error("haven't dealt with MemberDecl"); return null;}
    public IRClass.Expr visit(VarDecl node){
        if (node.expr != null) {
            IRClass.Expr expr = transformExpr(node.expr);
            assign(node.scope.localVariables.get(node.id), expr);
        }
        return null;
    }

    public IRClass.Expr visit(BlockStmt node){
        for (AST.Stmt stmt : node.stmts) visit(stmt);
        return null;
    }
    
    public IRClass.Expr visit(EmptyStmt node){return null;}
    
    public IRClass.Expr visit(ForStmt node){
        Label beglabel = new Label();
        Label bodyLabel = new Label();
        Label toDoLabel = new Label();
        Label endLabel = new Label();

        transformStmt(node.init);
        label(beglabel);
            if (node.condi != null)
                cjump(transformExpr(node.condi), bodyLabel, endLabel);
        label(bodyLabel);
            pushBreak(endLabel);
            pushContinue(toDoLabel);
            transformStmt(node.stmt);
            popBreak();
            popContinue();
        label(toDoLabel);
            transformStmt(node.step);
            jump(beglabel);
        label(endLabel);
        return null;
    }

    public IRClass.Expr visit(IfStmt node){
        Label thenLabel = new Label();
        Label elseLabel = new Label();
        Label endLabel  = new Label();
        IRClass.Expr condition = transformExpr(node.condition);
        
        if (node.elseStmt == null){
            cjump(condition, thenLabel, endLabel);
            label(thenLabel);
            transformStmt(node.stmt);
            label(endLabel);
        }
        else{
            cjump(condition, thenLabel, elseLabel);
            label(thenLabel);
            transformStmt(node.stmt);
            jump(endLabel);
            label(elseLabel);
            transformStmt(node.elseStmt);
            label(endLabel);
        }
        return null;
    }
    
    public IRClass.Expr visit(JumpStmt node){
        switch(node.jump){
            case BREAK:     jump(breakStack.getFirst());    break;
            case CONTINUE:  jump(continueStack.getFirst()); break;
            case RETURN:
                if (node.expr != null)
                    retun(transformExpr(node.expr));
                else
                    retun(null);
                break;
            default:
                System.out.println("Error occurs in IRTranslation of JumpStatement");
        }
        return null;
    }

    public IRClass.Expr visit(VarDeclStmt node){
        visit(node.varDecl);
        return null;
    }

    public IRClass.Expr visit(WhileStmt node){
        Label begLabel  = new Label();
        Label bodyLabel = new Label();
        Label endLabel  = new Label();
        label(begLabel);
        cjump(transformExpr(node.condition), bodyLabel, endLabel);
        label(bodyLabel);
        pushBreak(endLabel);
        pushContinue(begLabel);
        transformStmt(node.stmt);
        popBreak();
        popContinue();
        jump(begLabel);
        label(endLabel);
        return null;
    }

    public IRClass.Expr dealWithStringBinary(BinaryExpr node){
        LinkedList<IRClass.Expr> args = new LinkedList<>();
        args.add(transformExpr(node.lhs));
        args.add(transformExpr(node.rhs));
        switch (node.op){
            case ADD:
                return new Call(new Label("stringADD"), args);
            case LT:
                return new Call(new Label("stringLT"), args);
            case LE:
                return new Call(new Label("stringLE"), args);
            case GT:
                return new Call(new Label("stringGE"), args);
            case GE:
                return new Call(new Label("stringGE"), args);
            case EQ:
                return new Call(new Label("stringEQ"), args);
            case NE:
                return new Call(new Label("stringNE"), args);
            default:
                error("string Type has no such opeation!");
                return null;
        }
    }

    public IRClass.Expr visit(BinaryExpr node){
        if (node.op == BinaryExpr.BinaryOp.ASSIGN){
            return assignProcessing(node);
        }
        else if (node.lhs.returnType.name.equals("string")){
            return dealWithStringBinary(node);
        } else if (node.op == BinaryOp.LOGICAL_AND){
            Label thenLabel = new Label();
            Label elseLabel = new Label();
            Label endLabel  = new Label();
            Var tmpVar = allocateVar(node.scope);
            IRClass.Expr condition = transformExpr(node.lhs);
            cjump(condition, thenLabel, elseLabel);
            label(thenLabel);
            assign(tmpVar, transformExpr(node.rhs));
            jump(endLabel);
            label(elseLabel);
            assign(tmpVar, new Int(0));
            label(endLabel);
            return tmpVar;
        } else if (node.op == BinaryOp.LOGICAL_OR){
            Label thenLabel = new Label();
            Label elseLabel = new Label();
            Label endLabel  = new Label();
            Var tmpVar = allocateVar(node.scope);
            IRClass.Expr condition = transformExpr(node.lhs);
            cjump(condition, thenLabel, elseLabel);
            label(thenLabel);
            assign(tmpVar, new Int(1));
            jump(endLabel);
            label(elseLabel);
            assign(tmpVar, transformExpr(node.rhs));
            label(endLabel);
            return tmpVar;
        } else {
            IRClass.Expr lhs = transformExpr(node.lhs);
            IRClass.Expr rhs = transformExpr(node.rhs);
            return new Bin(node.op, lhs, rhs);
        }
    }

    public IRClass.Expr visit(BoolExpr node){ return new bool(node.value); }
    public IRClass.Expr visit(FunctionCallExpr node){
        Expr expr = node.functionName;
        LinkedList<IRClass.Expr> args = new LinkedList<>();

        if (expr instanceof IdentifierExpr){
            if (((IdentifierExpr) expr).isClassMember){
                args.add(node.scope.getLocal(((VariableType)node.scope.get("this", null)).varDecl.id));
//                addAll(args, node.parameters, node.scope);
                node.parameters.forEach(x -> args.add(transformExpr(x)));
                Call call = new Call(new Label(((IdentifierExpr) expr).className + ((IdentifierExpr) expr).Identifier)
                        , args);
                return makeCall(call, node.scope);
            }
            else{
//                addAll(args, node.parameters, node.scope);
                node.parameters.forEach(x -> args.add(transformExpr(x)));
                Call call = new Call (new Label(((IdentifierExpr) expr).Identifier), args);
                return makeCall(call, node.scope);
            }
        }
        else if (expr instanceof MemberAccess){
            String name = ((MemberAccess) expr).expr.returnType.name;
            args.add(transformExpr(((MemberAccess) expr).expr));
            node.parameters.forEach(x -> args.add(transformExpr(x)));
//            addOne(args, ((MemberAccess) expr).expr, node.scope);
//            addAll(args, node.parameters, node.scope);

            if (((MemberAccess) expr).name.equals("size")) name = "array";
            Call call = new Call (new Label(name + ((MemberAccess) expr).name), args);
            return makeCall(call, node.scope);
        }
        return null;
    }

    private IRClass.Expr makeCall(IRClass.Call call, Scope scope){
        if (isStmt()){
            call(call);
            return null;
        }
        else if (INLINE && inlineLevel == 0 && call.label.name.equals("func")){
            ++inlineLevel;
            IRClass.Expr tem = inline(call);
            --inlineLevel;
            return tem;
        }
        else {
            Var var = allocateVar(scope);
            assign(var, call);
            return var;
        }
    }

    private IRClass.Expr inline(Call call){
        FuncDecl funcDecl = (FuncDecl) allfuncs.get(call.label.name);
        if (funcDecl == null || !funcDecl.inlineable()) return call;
        HashMap<String, IRClass.Expr> varExprs = new HashMap<>();
        for (int i = 0; i < funcDecl.parameters.size(); ++i){
            varExprs.put(funcDecl.parameters.get(i).name, call.args.get(i));
        }
        return transformInline(transformExpr(((JumpStmt)funcDecl.blockStmt.stmts.get(0)).expr), varExprs);
        //adjusting
    }

    private IRClass.Expr transformInline(IRClass.Expr expr, HashMap<String, IRClass.Expr> varExprs){
        FunctionTransformer transformer = new FunctionTransformer(varExprs);
        transformer.visit(expr);
        return transformer.result();
    }

    public IRClass.Expr visit(IdentifierExpr node){
        if (node.isClassMember){
            Expr thisexpr = new ThisExpr();
            thisexpr.scope = node.scope;
            MemberAccess ma = new MemberAccess(thisexpr, node.Identifier);
            ma.memberOffset = node.classScope.variableOffset.get(node.Identifier);
            return visit(ma);
        }
        return node.scope.getLocal(((VariableType)node.returnType).varDecl.id);
    }

    public IRClass.Expr visit(IndexAccessExpr node){
        IRClass.Expr name = transformExpr(node.name);
        IRClass.Expr index = transformExpr(node.index);
        IRClass.Expr Addr = new Bin(BinaryOp.ADD, name, new Bin(BinaryOp.MUL, index, new Int(8)));
        return mem(Addr);
    }//type

    public IRClass.Expr visit(IntExpr node){return new Int(node.number);}
    public IRClass.Expr visit(LogicalExpr node){return new bool(node.value);}
    public IRClass.Expr visit(MemberAccess node){
        IRClass.Expr name = transformExpr(node.expr);
        IRClass.Expr offset = new Int(node.memberOffset);
        IRClass.Expr Addr = new Bin(BinaryOp.ADD, name, offset);
        return mem(Addr);
    }

    public IRClass.Expr visit(NewExpr node) {
        int dimension = node.temType.dimension;
        int exprNum = node.size.size();
        Var[] counter = new Var[exprNum];
        Var[] bound   = new Var[exprNum];
        Var[] allocateSize = new Var[exprNum];
        for (int i = 0; i < exprNum; ++i){
            counter[i] = allocateVar(node.scope);
            bound[i]   = allocateVar(node.scope);
            allocateSize[i] = allocateVar(node.scope);
            assign(bound[i], transformExpr(node.size.get(i)));
            assign(allocateSize[i], new Bin(BinaryOp.ADD,
                    new Bin(BinaryOp.MUL, bound[i], new Int(8)), new Int(8)));
        }
        Var tmp = allocateVar(node.scope);
        return allocateMem(tmp, 0, exprNum, dimension, counter, bound, allocateSize, node.temType.name, node.scope);
    }

    public IRClass.Expr visit(NullExpr node){
        return new Int(0);
    }// haven't dealt with the problem
    public IRClass.Expr visit(PostUnaryExpr node){
        BinaryOp op = node.op == UnaryExpr.UnaryOps.PPLUS ? BinaryOp.ADD : BinaryOp.SUB;
        IRClass.Expr operand = transformExpr(node.expr);
        if (isStmt()){
            assign(operand, new Bin(op, operand, new Int(1)));
            return null;
        }
        else if (operand instanceof Var) {
            Var tmp = allocateVar(node.scope);
            assign(tmp, operand);
            assign(operand, new Bin(op ,tmp, new Int(1)));
            return tmp;
        }
        else{
            Var a = allocateVar(node.scope);
            Var v = allocateVar(node.scope);
            assign(a, operand);
            assign(v, a);
            assign(a, new Bin(op, a, new Int(1)));
            return v;
        }
    }

    public IRClass.Expr visit(StringExpr node){
        Label stringLabel = new Label();
        Str str = new Str(node.content, stringLabel);
        GlobalStringList.add(str);
        return str;
    }

    public IRClass.Expr visit(ThisExpr node){
        return node.scope.getLocal(((VariableType)node.scope.get("this", null)).varDecl.id);
    }
    public IRClass.Expr visit(UnaryExpr node){
        IRClass.Expr expr = transformExpr(node.expr);
        switch(node.op){
            case PLUS:
                return expr;
            case MINUS:case BANG:case SIM:
                return new Uni(node.op, expr);
            case PPLUS:
                if (isStmt()){
                    assign(expr, new Bin(BinaryOp.ADD, expr, new Int(1)));
                    return null;
                }
                else
                    assign(expr, new Bin(BinaryOp.ADD, expr, new Int(1)));
                    return expr;
            case MMINUS:
                if (isStmt()){
                    assign(expr, new Bin(BinaryOp.SUB, expr, new Int(1)));
                    return null;
                }
                else
                    assign(expr, new Bin(BinaryOp.SUB, expr, new Int(1)));
                return expr;
            default:
                error("error in IR Unary");
                return null;
        }
    }

    private void cjump(IRClass.Expr expr, Label thenLabel, Label elseLabel){ stmts.add(new CJump(expr, thenLabel, elseLabel)); }
    
    private void label(Label label){ stmts.add(new LabelStmt(label)); }

    private void jump(Label label){ stmts.add(new Jump(label)); }

    private void pushBreak(Label label){ breakStack.push(label); }

    private void popBreak(){ breakStack.pop(); }

    private void pushContinue(Label label){ continueStack.push(label); }

    private void popContinue(){ continueStack.pop(); }

    private void retun(IRClass.Expr expr){stmts.add(new Return(expr));}

    private void exprStmt(IRClass.Expr _expr){ stmts.add(new ExprStmt(_expr));}

    private IRClass.Expr addressof(IRClass.Expr lhs){
        if (lhs instanceof Var)
            return new Addr(((Var) lhs).type, ((Var) lhs).entity);
        else if (lhs instanceof Mem)
            return ((Mem) lhs).expr;
        else error("Error in addressof!!");
        return null;
    }

    private void assign(IRClass.Expr lhs, IRClass.Expr rhs){ stmts.add(new Assign(addressof(lhs), rhs)); }

    private IRClass.Expr assignProcessing(BinaryExpr node){
        IRClass.Expr rhs = transformExpr(node.rhs);
        if (isStmt()){
            assign(transformExpr(node.lhs), rhs);
            return null;
        }
        else{
            Var tmp = allocateVar(node.scope);
            assign(tmp, rhs);
            assign(transformExpr(node.lhs), tmp);
            return tmp;
        }
    }

    private Mem mem(IRClass.Expr expr){
        return new Mem(expr);
    }

    private Var allocateVar(Scope scope){
        int id = -VarDecl.counter++;
        Var tem = new Var(INT64, new Entity("@tmp"+id));
        scope.localVariables.put(id, tem);
        return tem;
    }

    private Var allocateVar(Scope scope, int id, String name){
        Var tem = new Var(INT64, new Entity(name));
        scope.localVariables.put(id, tem);
        return tem;
    }

    private IRClass.Expr malloc(IRClass.Expr size, Scope scope){
        LinkedList<IRClass.Expr> args = new LinkedList<>();
        args.add(size);
        return new Call(new Label("malloc"), args);
    }

    private IRClass.Expr allocateMem(IRClass.Expr expr, int current, int exprWhole, int whole,
                                     Var[] counter, Var[] bound, Var[] allocateSize, String className, Scope scope){
        if (current < exprWhole){
            assign(expr, malloc(allocateSize[current], scope));
            assign(mem(expr), bound[current]);
            assign(expr, new Bin(BinaryOp.ADD, expr, new Int(8)));
            if (current == exprWhole - 1 && exprWhole < whole) return expr;
        }
        else if (exprWhole == whole) {
            if (className.equals("int") || className.equals("bool")){
                assign(expr, new Int(0));
            }
            else{
                int size = res.typeTable.get(className).size;
                assign(expr, malloc(new Int(size), scope));
                Decl funcOrCons = allfuncs.get(className + className);
                if (funcOrCons != null){
                    LinkedList<IRClass.Expr> args = new LinkedList<>();
                    args.add(expr);
                    call(new Label(className + className), args);
                } else {
                    //adjusting
                }
            }
            return expr;
        }
        else return new Int(0);

        Label beglabel = new Label();
        Label bodyLabel = new Label();
        Label toDoLabel = new Label();
        Label endLabel = new Label();
        assign(counter[current], new Int(0));
        label(beglabel);
            cjump(new Bin(BinaryOp.LT, counter[current], bound[current]), bodyLabel, endLabel);
        label(bodyLabel);
        if (current < exprWhole)
            allocateMem(
                    mem(new Bin(BinaryOp.ADD, expr, new Bin(BinaryOp.MUL, counter[current], new Int(8)))),
                    current + 1, exprWhole, whole, counter, bound, allocateSize, className, scope
            );
        label(toDoLabel);
            assign(counter[current], new Bin(BinaryOp.ADD, counter[current], new Int(1)));
            jump(beglabel);
        label(endLabel);
        return expr;
    }

    private void call(Call _call){
        stmts.add(new ExprStmt(_call));
    }

    private void call(Label _label, LinkedList<IRClass.Expr> _args){
        stmts.add(new ExprStmt(new Call(_label, _args)));
    }
/*
    private void addAll(LinkedList<IRClass.Expr> args, LinkedList<Expr> parameters, Scope scope){
        Var var;
        for (Expr expr : parameters){
            var = allocateVar(scope);
            assign(var, transformExpr(expr));
            args.add(var);
        }
    }

    private void addOne(LinkedList<IRClass.Expr> args, Expr parameter, Scope scope){
        Var var = allocateVar(scope);
        assign(var, transformExpr(parameter));
        args.add(var);
    }

    private void addOne(LinkedList<IRClass.Expr> args, IRClass.Expr parameter, Scope scope){
        Var var = allocateVar(scope);
        assign(var, parameter);
        args.add(var);
    }
    */
}