package ASTVisitor;

import AST.*;

/**
 * Created by fzy on 17-4-3.
 */
public class PrintVisitor implements IASTVisitor<Void> {

    public Void visit(ASTnode node){ if (node != null) node.accept(this);return null;}
    public Void visit(Decl node){ if (node != null) node.accept(this);return null;}
    public Void visit(Stmt node){ if (node != null) node.accept(this);return null;}
    public Void visit(Expr node){ if (node != null) node.accept(this);return null;}

    public Void visit(Program node){
        System.out.println("programStart");
        node.classDecls.forEach(this::visit);
        node.funcDecls.forEach(this::visit);
        node.varDecls.forEach(this::visit);
//        node.decls.forEach(x -> visit(x));
        return null;
    }

    public Void visit(ClassDecl node){
        System.out.println("class "+node.className);
        node.vars.forEach(x -> visit(x));
        node.funcs.forEach(x -> visit(x));
        if (node.cons != null) visit(node.cons);
        return null;
    }

    public Void visit(ConsDecl node){
        System.out.println("constructor:" + node.funcName);
        System.out.println("beginParameters");
        if (node.parameters != null)
            node.parameters.forEach(x -> visit(x));
        System.out.println("endParameters");
        visit(node.blockStmt);
        return null;
    }

    public Void visit(FuncDecl node){
        System.out.println("function:" + node.temtype.name + ' ' + node.temtype.dimension + ' ' + node.funcName);
        System.out.println("beginParameters");
        if (node.parameters != null)
            node.parameters.forEach(x -> visit(x));
        System.out.println("endParameters");
        visit(node.blockStmt);
        return null;
    }

    public Void visit(MemberDecl node){
        System.out.println("ahahahahahahahahahahaha");
        return null;
    }

    public Void visit(VarDecl node){
        System.out.print("var:"+node.temType.name + ' ' + node.temType.dimension + ' ' + node.name);
        visit(node.expr);
        return null;
    }

    public Void visit(BlockStmt node){
        System.out.println("beginBlock");
        if (node.stmts != null)
            node.stmts.forEach(x -> visit(x));
        System.out.println("\nendBlock");
        return null;
    }

    public Void visit(EmptyStmt node){
        System.out.println("EmptyStmt");
        return null;
    }

    public Void visit(ForStmt node){
        System.out.println("for");
        visit(node.init);
        visit(node.condi);
        visit(node.step);
        visit(node.stmt);
        return null;
    }

    public Void visit(IfStmt node){
        System.out.println("if");
        visit(node.condition);
        visit(node.stmt);
        System.out.println("else");
        visit(node.elseStmt);
        return null;
    }

    public Void visit(JumpStmt node){
        System.out.print("jump:" + node.jump + " ");
        visit(node.expr);
        return null;
    }

    public Void visit(VarDeclStmt node){
        visit(node.varDecl);
        System.out.println();
        return null;
    }

    public Void visit(WhileStmt node){
        System.out.println("while");
        visit(node.condition);
        visit(node.stmt);
        return null;
    }

    public Void visit(BinaryExpr node){
     //   System.out.print("Binary:");
        visit(node.lhs);
        System.out.print("op" + node.op + ' ');
        visit(node.rhs);
        return null;
    }

    public Void visit(BoolExpr node){
        System.out.print("Bool:");
        System.out.print(node.value + " ");
        return null;
    }

    public Void visit(FunctionCallExpr node){
        System.out.print("FuncCall:");
        visit(node.functionName);
        System.out.println("beginParameters");
        if (node.parameters != null)
            node.parameters.forEach(x -> visit(x));
        System.out.println("endParameters");
        return null;
    }

    public Void visit(IdentifierExpr node){
        System.out.print("Identifier:");
        System.out.print(node.Identifier + " ");
        return null;
    }

    public Void visit(IndexAccessExpr node){
        System.out.print("IndexAccess:");
        visit(node.name);
        visit(node.index);
        return null;
    }

    public Void visit(IntExpr node){
        System.out.print("int:");
        System.out.print(node.number + " ");
        return null;
    }

    public Void visit(LogicalExpr node){
        System.out.print("logic:");
        System.out.print(node.value + " ");
        return null;
    }

    public Void visit(MemberAccess node){
        System.out.print("MemberAccess:");
        visit(node.expr);
        System.out.print('.');
        System.out.println(node.name + " ");
        return null;
    }

    public Void visit(NewExpr node){
        System.out.print("= New " + node.temType.name + ' ' + node.temType.dimension + ' ');
        if (node.size != null)
            node.size.forEach(x -> visit(x));
        return null;
    }

    public Void visit(NullExpr node){
        System.out.print("null ");
        return null;
    }

    public Void visit(PostUnaryExpr node){
        System.out.print("PostUnary");
        visit(node.expr);
        System.out.print("op" + node.op + " ");
        return null;
    }

    public Void visit(StringExpr node){
        System.out.print("String:");
        System.out.print(node.content + " ");
        return null;
    }

    public Void visit(ThisExpr node){
        System.out.print("this ");
        return null;
    }

    public Void visit(UnaryExpr node){
        System.out.print("Unary:");
        System.out.print("op" + node.op + " ");
        visit(node.expr);
        return null;
    }
}