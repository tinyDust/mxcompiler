package ASTVisitor;

import AST.*;
import TypeAndSymbol.*;

import java.util.LinkedList;

import static AST.JumpStmt.jumps.RETURN;
import static ASTVisitor.environment.*;

/**
 * Created by fzy on 17-4-4.
 */

public class CheckerVisitor implements IASTVisitor<Type> {
    private ResolverVisitor res;
    private boolean showProcess = false;
    private LinkedList<environment> currentEnvironment;
    private String currentClassName = null;
    private Scope currentClassScope = null;

    public CheckerVisitor(ResolverVisitor _res){
        currentEnvironment = new LinkedList<>();
        currentEnvironment.push(WHOLE);
        res = _res;
    }

    private void except(String wrongInfo){
        System.out.println(wrongInfo);
        System.exit(1);
    }

    private VariableType getVariableType(Type node){
        if (! (node instanceof VariableType)) except("not a variable type");
        return (VariableType) node;
    }

    private FunctionType getFunctionType(Type node){
        if (! (node instanceof FunctionType)) except("not a function type");
        return (FunctionType) node;
    }

    private void checkParameters(FunctionType f, FunctionCallExpr node){
        if (f.parameters.size() != node.parameters.size()) except("function parameter numbers not equal");
        else {
            int len = f.parameters.size();
            for (int i = 0; i < len; ++i){
                VariableType tem = getVariableType(visit(node.parameters.get(i)));
                if (!(f.parameters.get(i).equal(tem) || !f.parameters.get(i).isPrimitive() && tem.name.equals("null")))
                    except("function parameter type not match");
            }
        }
    }

    public Type visit(ASTnode node) { if (node != null) return (Type)node.accept(this); return null;}
    public Type visit(Decl node)    { if (node != null) return (Type)node.accept(this); return null;}
    public Type visit(Stmt node)    { if (node != null) return (Type)node.accept(this); return null;}
    public Type visit(Expr node)    { if (node != null) return (Type)node.accept(this); return null;}

    public Type visit(Program node){
        node.varDecls.forEach(x -> visit(x));
        node.classDecls.forEach(x -> visit(x));
        node.funcDecls.forEach(x -> visit(x));
        return null;
    }

    public Type visit(ClassDecl node){
        currentEnvironment.push(CLASS);
        currentClassName = node.className;
        currentClassScope = node.scope;
        node.vars.forEach(x -> visit(x));
        node.funcs.forEach(x -> visit(x));
        if (node.cons != null) visit(node.cons);
        currentClassScope = null;
        currentClassName = null;
        currentEnvironment.pop();
        return null;
    }

    public Type visit(ConsDecl node){
        if (showProcess) System.out.println(node.myName);
        currentEnvironment.push(FUNC);
        if(node.parameters != null) node.parameters.forEach(x -> visit(x));
        visit(node.blockStmt);
        currentEnvironment.pop();
        return null;
    }

    public Type visit(FuncDecl node){
        if (showProcess) System.out.println(node.myName);
        currentEnvironment.push(FUNC);
        if(node.parameters != null) node.parameters.forEach(x -> visit(x));
        visit(node.blockStmt);
        currentEnvironment.pop();
        return null;
    }

    public Type visit(MemberDecl node){return null;}
    public Type visit(VarDecl node){
        if (showProcess) System.out.println(node.myName);
        if (currentEnvironment.getFirst() != CLASS) {
            if (node.temType.name.equals("void")) except("void type in definition");
            if (res.typeTable.get(node.temType.name) == null) except("not exist type in definition");
    //adjusted        node.scope.put(node.name, new VariableType(node.temType));
        }

        VariableType tem1 = new VariableType(node.temType, null);
        VariableType tem2 = (VariableType) visit(node.expr);
        if (tem2 != null){
            if (!(tem2.name.equals("null") && !tem1.isPrimitive() || tem1.equal(tem2))) except("no match type in VarDecl");
        }
        return null;
    }

    public Type visit(BlockStmt node){
        if (showProcess) System.out.println(node.myName);
        if (node.stmts != null) node.stmts.forEach(x -> visit(x));
        return null;
    }

    public Type visit(EmptyStmt node){return null;}

    public Type visit(ForStmt node){
        if (showProcess) System.out.println(node.myName);
        visit(node.init);
        VariableType condi = (VariableType) visit(node.condi);
        if (condi != null && !(condi.name.equals("bool") && condi.dimension == 0)) except("wrong condition in ifStmt");
        visit(node.step);
        visit(node.stmt);
        return null;
    }

    public Type visit(IfStmt node){
        VariableType tem = getVariableType(visit(node.condition));
        if (!tem.name.equals("bool")  || tem.dimension != 0) except("expect bool value in ifstmt");
        visit(node.stmt);
        visit(node.elseStmt);
        return null;
    }

    public VariableType visit(JumpStmt node){
        if (showProcess) System.out.println(node.myName);
        Type n = visit(node.expr);
        if (node.jump == RETURN &&
                !(n == null && node.returnType.name.equals("void") ||
                        n != null && (node.returnType.equal(getVariableType(n)) || n.name.equals("null") && !node.returnType.isPrimitive())))
            except("incorrect return type");
        return null;
    }

    public Type visit(VarDeclStmt node){
        if (showProcess) System.out.println(node.myName);
        visit(node.varDecl);
        return null;
    }

    public Type visit(WhileStmt node){
        if (showProcess) System.out.println(node.myName);
        VariableType tem = getVariableType(visit(node.condition));
        if (!tem.name.equals("bool") || tem.dimension != 0) except("expect bool value in whilestmt");
        visit(node.stmt);
        return null;
    }

    public Type visit(BinaryExpr node){
        if (showProcess) System.out.println(node.myName);
        VariableType lhs = getVariableType(visit(node.lhs));
        VariableType rhs = getVariableType(visit(node.rhs));

        switch(node.op){
            case EQ: case NE:
                if (!(lhs.isPrimitive() && lhs.equal(rhs) || lhs.name.equals("null") && !rhs.isPrimitive()
                || rhs.name.equals("null") && !lhs.isPrimitive())) except("Wrong in op EQ");
                return node.returnType = new VariableType("bool", 0, null,true);

            case GE: case GT: case LE: case LT:
                if (!((lhs.name.equals("int") || lhs.name.equals("string")) && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op int only");
                return node.returnType = new VariableType("bool", 0, null,true);

            case MOD: case DIV: case MUL: case SHL: case XOR:
            case SHR: case SUB: case BITWISE_OR: case BITWISE_AND:
                if (!(lhs.name.equals("int") && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op int only");
                return node.returnType = new VariableType("int", 0, null,true);

            case ADD:
                if (!((lhs.name.equals("int") || lhs.name.equals("string")) && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op add");
                return node.returnType = new VariableType(lhs.name, 0,null, true);

            case ASSIGN:
                if (!(!lhs.isConstant && (lhs.equal(rhs) || !lhs.isPrimitive() && rhs.name.equals("null")))) except("Wrong in ASSIGN");
                return node.returnType = new VariableType("void", 0, null,true);

            case LOGICAL_OR: case LOGICAL_AND:
                if (!(lhs.name.equals("bool") && lhs.dimension == 0 && lhs.equal(rhs))) except("Wrong in logical && ||");
                return node.returnType = new VariableType("bool", 0, null,true);
            default:
                except("Not a valid op");
                return null;
        }
    }

    public Type visit(BoolExpr node){
        if (showProcess) System.out.println(node.myName);
        return node.returnType = new VariableType("bool", 0, null,true);
    }

    public Type visit(FunctionCallExpr node){
        FunctionType tem = getFunctionType(visit(node.functionName));
        checkParameters(tem, node);
//        node.asFuncName = node.functionName.asFuncName;
        return node.returnType = new VariableType(tem.returnType,null, true);
    }

    public Type visit(IdentifierExpr node){
        wrapper wrp = new wrapper();
        Type tem = node.scope.get(node.Identifier, wrp);
        if (tem == null) except("undefined variable or function " + node.Identifier);
        node.type = tem;
        if (wrp.envir == CLASS) {
            node.isClassMember = true;
            node.className = currentClassName;
            node.classScope = currentClassScope;
        }
        if (tem instanceof VariableType)
            return node.returnType = tem;
        else if (tem instanceof FunctionType)
            return node.returnType = tem;
        return null;
    }

    public Type visit(IndexAccessExpr node){
        if (showProcess) System.out.println(node.myName);
        VariableType tem = getVariableType(visit(node.name));
        if (tem.dimension == 0) except("not a valid type in Index Access");
        return node.returnType = new VariableType(tem.name, tem.dimension - 1, null,false);
    }

    public Type visit(IntExpr node){
        return node.returnType = new VariableType("int", 0, null,true);
    }

    public Type visit(LogicalExpr node){
        return node.returnType = new VariableType("bool", 0, null,true);
    }

    public Type visit(MemberAccess node){
        if (showProcess) System.out.println(node.myName);
        VariableType tem = getVariableType(visit(node.expr));

        //get the class that node.name is in
        ClassDecl tem2 = res.typeTable.get(tem.name);
        if (tem2 == null) except("not a class in MemberAccess");

        //array type has a field called size
        if (tem.dimension > 0){
            if (node.name.equals("size")) return node.returnType = new FunctionType("int", null);
            else except("the array type has no such members");
        }


        if (tem2.scope == null) except("the class has no members");
        if (!tem2.scope.hasKey(node.name)) except("the class has no such member " + node.name);
        Type tem3 = tem2.scope.get(node.name, null);
        if (tem3 instanceof VariableType) {
            VariableType tem4 = (VariableType) tem3;
            tem4.isConstant = tem.isConstant;
            node.memberOffset = tem2.scope.variableOffset.get(node.name);
            return node.returnType = tem4;
        }
        else{
            node.asFuncName = tem3.name + node.name;
        }

        return node.returnType = tem3;
    }

    public Type visit(NewExpr node){
        if (showProcess) System.out.println(node.myName);
        return node.returnType = new VariableType(node.temType, null);
    }

    public Type visit(NullExpr node){
        return node.returnType = new VariableType("null", 0, null,true);
    }
    public Type visit(PostUnaryExpr node){
        if (showProcess) System.out.println(node.myName);
        VariableType tem = getVariableType(visit(node.expr));
        if (tem == null) except("null occurs in PostUnaryOp");
        switch(node.op){
            case MMINUS: case PPLUS:
                if (!(tem.name.equals("int") && tem.dimension == 0 && !tem.isConstant)) except("Wrong in ++--");
                return node.returnType = new VariableType("int", 0, null,true);
            default: except("not a valid op in PostUnary"); return null;
        }
    }

    public Type visit(StringExpr node){
        return node.returnType = new VariableType("string", 0,null, true);
    }

    //adjusting
    public Type visit(ThisExpr node){
        return node.returnType = new VariableType(node.className, 0, null,false);
    }

    public Type visit(UnaryExpr node){
        if (showProcess) System.out.println(node.myName);
        VariableType tem = getVariableType(visit(node.expr));
        if(tem == null) except("null occurs in UnaryOp");
        switch(node.op){
            case PPLUS: case MMINUS:
                if (!(tem.name.equals("int") && tem.dimension == 0 && !tem.isConstant)) except("Wrong in ++--");
                return node.returnType = new VariableType("int", 0, null,false);
            case PLUS:case SIM:case MINUS:
                if (!(tem.name.equals("int")  && tem.dimension == 0)) except("Wrong in +-~");
                return node.returnType = new VariableType("int", 0, null,true);
            case BANG:
                if (!(tem.name.equals("bool") && tem.dimension == 0)) except("Wrong in !");
                return node.returnType = new VariableType("bool", 0, null,true);
            default: except("Not a unary op!"); return null;
        }
    }
}
