package ASTVisitor;

import AST.*;
import TypeAndSymbol.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static ASTVisitor.environment.*;

/**
 * Created by fzy on 17-4-4.
 */
public class ResolverVisitor implements IASTVisitor<Void> {
    public Map<String, ClassDecl> typeTable;
    public LinkedList<Scope> scopes;
    public Scope currentScope;
    private LinkedList<environment> currentEnvironment;
    private VariableType currentReturnType;
    private String currentClassName = null;
    private boolean justFunc = false;
    private HashMap<String, Decl> allfuncs;
    private int ResolveTime;

    private void addClass(String className, ClassDecl CD){
        if (typeTable.containsKey(className)) except("redefined class");
        typeTable.put(className, CD);
    }

    private void enterClass(String name){
        currentEnvironment.push(CLASS);
        currentClassName = name;
    }

    private void exitClass(){
        currentEnvironment.pop();
        currentClassName = null;
    }

    private void except(String wrongInfo){
        System.out.println(wrongInfo);
        System.exit(1);
    }

    private void addBuiltInFunc(String name, String ret, String parameter){
        FunctionType func = new FunctionType();
        func.name = name;
        func.returnType = new VariableType(ret, 0, null,true);
        if (parameter != null) func.parameters.add(new VariableType(parameter, 0, null,true));
        currentScope.put(name, func);
    }

    private void builtInInit(){
        addBuiltInFunc("print", "void", "string");
        addBuiltInFunc("println", "void", "string");
        addBuiltInFunc("getString", "string", null);
        addBuiltInFunc("getInt", "int", null);
        addBuiltInFunc("toString", "string", "int");
        addBuiltInFunc("printInt", "void", "int");

        typeTable.put("int" , new ClassDecl(null));
        typeTable.put("bool", new ClassDecl(null));
        typeTable.put("void", new ClassDecl(null));

        Scope stringScope = new Scope(currentScope, currentEnvironment.getFirst());
        stringScope.put("length", new FunctionType("int", null));
        stringScope.put("parseInt", new FunctionType("int", null));
        stringScope.put("ord", new FunctionType("int", "int"));
        stringScope.put("substring", new FunctionType("string", "int", "int"));
        typeTable.put("string", new ClassDecl(stringScope));
    }


    public ResolverVisitor(){
        scopes = new LinkedList<>();
        currentEnvironment = new LinkedList<>();
        currentEnvironment.push(WHOLE);
        currentScope = new Scope(null, currentEnvironment.getFirst());
        typeTable = new HashMap<>();
        scopes.push(currentScope);
        currentReturnType = null;
        builtInInit();
    }

    private void enterScope(Scope scope){
        currentScope = scope;
        scopes.push(scope);
    }

    private void exitScope(){
        currentScope = scopes.pop().parent;
    }

    private void checkEnvironment(JumpStmt node) {
        if (currentEnvironment.getFirst() != LOOP && (node.jump == JumpStmt.jumps.CONTINUE || node.jump == JumpStmt.jumps.BREAK))
            except("jump stmt should not occur here");
    }

    public Void visit(ASTnode node){ if (node != null) node.accept(this);return null;}
    public Void visit(Decl node){ if (node != null) node.accept(this);return null;}
    public Void visit(Stmt node){ if (node != null) node.accept(this);return null;}
    public Void visit(Expr node){ if (node != null) node.accept(this);return null;}

    public Void visit(Program node){
        allfuncs = node.allfuncs;
        node.scope = currentScope;

        ResolveTime = 1;
        node.classDecls.forEach(x -> {
            x.scope = new Scope(currentScope, CLASS);
            addClass(x.className, x);}
        );

        node.classDecls.forEach(x -> visit(x));
        node.funcDecls.forEach(x -> visit(x));

        ResolveTime = 2;
        for (Decl decl : node.Decls){
            visit(decl);
        }

        Type mainType = currentScope.get("main", null);
        if (mainType == null || !(mainType instanceof FunctionType)) except("no main function");
        else if (!(((FunctionType) mainType).returnType.name.equals("int")
                && ((FunctionType) mainType).returnType.dimension == 0)) except("wrong return type of main");
        else if (((FunctionType) mainType).parameters.size() != 0) except("main func should not have parameters");

        scopes.pop();
        return null;
    }

    public Void visit(ClassDecl node){
        enterScope(node.scope);
        enterClass(node.className);

        node.vars.forEach(x -> visit(x));
        node.funcs.forEach(x -> visit(x));
        if (node.cons != null) visit(node.cons);

        exitClass();
        exitScope();
        node.scope.refresh();// memberOffset initiation
        return null;
    }

    public Void visit(ConsDecl node){
        if (ResolveTime == 1){
            allfuncs.put(currentClassName + node.funcName, node);
            currentScope.put(node.funcName, new FunctionType(node));
            return null;
        }
        else{
            currentEnvironment.push(FUNC);
            node.scope = new Scope(currentScope,  currentEnvironment.getFirst());
            enterScope(node.scope);
            currentReturnType = new VariableType("Constructor", 0, null,true);
            justFunc = true;

            if (!node.funcName.equals(currentClassName)) except("not a valid constructor");
//          if (node.parameters != null) node.parameters.forEach(x -> visit(x));
            visit(node.blockStmt);

            currentReturnType = null;
            currentEnvironment.pop();
            exitScope();
            return null;

        }
    }

    public Void visit(FuncDecl node){
        if (ResolveTime == 1){
            currentScope.put(node.funcName, new FunctionType(node));
            if (currentClassName == null)
                allfuncs.put(node.funcName, node);
            else
                allfuncs.put(currentClassName + node.funcName, node);
            return null;
        }
        else{
            currentReturnType = ((FunctionType) currentScope.get(node.funcName, null)).returnType;
            currentEnvironment.push(FUNC);
            node.scope = new Scope(currentScope, currentEnvironment.getFirst());
            enterScope(node.scope);
            justFunc = true;

            if (typeTable.get(currentReturnType.name) == null) except("return type not exist ");
            if (node.parameters != null) node.parameters.forEach(x -> visit(x));
            visit(node.blockStmt);

            currentEnvironment.pop();
            currentReturnType = null;
            exitScope();
            return null;
        }
    }

    public Void visit(MemberDecl node){
        System.out.println("impossible reach");
        System.exit(1);
        return null;
    }

    public Void visit(VarDecl node){
        if (ResolveTime == 1){
            if (node.temType.name.equals("void")) except("void type in definition");
            if (typeTable.get(node.temType.name) == null) except("not exist type in definition");
            currentScope.put(node.name, new VariableType(node.temType, node));
            node.scope = currentScope;
            return null;
        }
        else if (ResolveTime == 2 && currentEnvironment.getFirst() == CLASS) return null;
        else{
            node.scope = currentScope;
            if (node.temType.name.equals("void")) except("void type in definition");
            if (typeTable.get(node.temType.name) == null) except("not exist type in definition");
            if (node.expr != null) {
                visit(node.expr);
                VariableType type = (VariableType) node.expr.returnType;
                if (!(type.name.equals("null") && !node.temType.isPrimitive() ||
                        type.equal(node.temType)))
                    except("no match type in VarDecl");
            }
            currentScope.put(node.name, new VariableType(node.temType, node));
            return null;
        }
    }

    public Void visit(BlockStmt node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        boolean hasCreateNewScope = true;
        if (!justFunc) enterScope(new Scope(currentScope, currentEnvironment.getFirst()));
        else {
            justFunc = false;
            hasCreateNewScope = false;
        }
        if(node.stmts != null) node.stmts.forEach(x -> visit(x));
        if(hasCreateNewScope) exitScope();
        return null;
    }

    public Void visit(ForStmt node){
        assert (ResolveTime == 2);

        currentEnvironment.push(LOOP);
        node.scope = currentScope;

        visit(node.init);

        visit(node.condi);
        if (node.condi != null){
            VariableType condi = (VariableType) node.condi.returnType;
            if (!(condi.name.equals("bool") && condi.dimension == 0)) except("wrong condition in ForStmt");
        }

        visit(node.step);

        enterScope(new Scope(currentScope, currentEnvironment.getFirst()));
        justFunc = true;

        visit(node.stmt);

        justFunc = false;
        exitScope();
        currentEnvironment.pop();
        return null;
    }

    public Void visit(IfStmt node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        visit(node.condition);

        VariableType condi = (VariableType) node.condition.returnType;
        if (!(condi.name.equals("bool") && condi.dimension == 0)) except("wrong condition in ForStmt");

        enterScope(new Scope(currentScope, currentEnvironment.getFirst()));
        justFunc = true;
        visit(node.stmt);
        justFunc = false;
        exitScope();

        if (node.elseStmt != null){
            enterScope(new Scope(currentScope, currentEnvironment.getFirst()));
            justFunc = true;
            visit(node.elseStmt);
            justFunc = false;
            exitScope();
        }
        return null;
    }

    public Void visit(JumpStmt node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        if (node.jump == JumpStmt.jumps.RETURN) {
            if (currentReturnType == null) except("Return appears not in a function");
            node.returnType = currentReturnType;

            if (node.expr == null){
                if (!(currentReturnType.name.equals("Constructor") || currentReturnType.name.equals("void")))
                    except("missing return type in return");
            }
            else{
                visit(node.expr);
                VariableType type = (VariableType) node.expr.returnType;
                if (!(currentReturnType.equal(type) || !currentReturnType.isPrimitive() && type.name.equals("null")))
                    except("Return Type Not Match!");
            }
        }
        else checkEnvironment(node);

        return null;
    }

    public Void visit(VarDeclStmt node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        visit(node.varDecl);
        return null;
    }

    public Void visit(WhileStmt node){
        assert (ResolveTime == 2);
        currentEnvironment.push(LOOP);
        node.scope = currentScope;
        visit(node.condition);
        VariableType condi = (VariableType) node.condition.returnType;
        if (!(condi.name.equals("bool") && condi.dimension == 0)) except("wrong condition in ForStmt");

        enterScope(new Scope(currentScope, currentEnvironment.getFirst()));
        justFunc = true;
        visit(node.stmt);
        justFunc = false;
        currentEnvironment.pop();
        exitScope();

        return null;
    }

    public Void visit(BinaryExpr node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        visit(node.lhs);
        visit(node.rhs);

        VariableType lhs = (VariableType) node.lhs.returnType;
        VariableType rhs = (VariableType) node.rhs.returnType;
        switch(node.op){
            case EQ: case NE:
                if (!(lhs.isPrimitive() && lhs.equal(rhs) || lhs.name.equals("null") && !rhs.isPrimitive()
                        || rhs.name.equals("null") && !lhs.isPrimitive())) except("Wrong in op EQ");
                node.returnType = new VariableType("bool", 0, null,true);
                break;

            case GE: case GT: case LE: case LT:
                if (!((lhs.name.equals("int") || lhs.name.equals("string")) && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op int only");
                node.returnType = new VariableType("bool", 0, null,true);
                break;

            case MOD: case DIV: case MUL: case SHL: case XOR:
            case SHR: case SUB: case BITWISE_OR: case BITWISE_AND:
                if (!(lhs.name.equals("int") && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op int only");
                node.returnType = new VariableType("int", 0, null,true);
                break;

            case ADD:
                if (!((lhs.name.equals("int") || lhs.name.equals("string")) && lhs.dimension == 0 && lhs.equal(rhs)))
                    except("Wrong in op add");
                node.returnType = new VariableType(lhs.name, 0, null,true);
                break;

            case ASSIGN:
                if (!(!lhs.isConstant && (lhs.equal(rhs) || !lhs.isPrimitive() && rhs.name.equals("null")))) except("Wrong in ASSIGN");
                node.returnType = new VariableType("void", 0, null,true);
                break;

            case LOGICAL_OR: case LOGICAL_AND:
                if (!(lhs.name.equals("bool") && lhs.dimension == 0 && lhs.equal(rhs))) except("Wrong in logical && ||");
                node.returnType = new VariableType("bool", 0, null,true);
                break;

            default:
                except("Not a valid op");
                return null;
        }
        return null;
    }

    private void checkParameters(List<VariableType> stardard, List<Expr> test){
        if (stardard.size() != test.size()) except("size not match in function call");
        else {
            for (int i = 0; i < stardard.size(); ++i){
                VariableType tem = (VariableType)test.get(i).returnType;
                if (!(stardard.get(i).equal(tem) || !stardard.get(i).isPrimitive() && tem.name.equals("null")))
                    except("function parameter type not match");
            }
        }
    }

    public Void visit(FunctionCallExpr node){
        assert (ResolveTime == 2);
        //adjusting :
        node.scope = currentScope;

        visit(node.functionName);
        if (node.parameters != null)
            node.parameters.forEach(x -> visit(x));

        FunctionType func = (FunctionType) node.functionName.returnType;
        checkParameters(func.parameters, node.parameters);

        node.returnType = new VariableType(func.returnType.name, func.returnType.dimension, null,true);
        return null;
    }

    public Void visit(IdentifierExpr node){
        assert (ResolveTime == 2);

        wrapper wrp = new wrapper();
        node.scope = currentScope;

        Type type = node.scope.get(node.Identifier, wrp);
        if (wrp.envir == CLASS) {
            node.isClassMember = true;
            node.className = currentClassName;
            node.classScope = typeTable.get(node.className).scope;
        }

        if (type == null){
            except("Identifier : " + node.Identifier + " is undefined");
        }
        else node.returnType = type;
        return null;
    }

    public Void visit(IndexAccessExpr node){
        assert (ResolveTime == 2);
        node.scope = currentScope;
        visit(node.name);
        visit(node.index);

        VariableType lhsType = (VariableType) node.name.returnType;
        if (lhsType.dimension == 0) except("not a array type, should not access index");

        VariableType rhsType = (VariableType) node.index.returnType;
        if (!(rhsType.name.equals("int") && rhsType.dimension == 0)) except("not a valid int index");

        node.returnType = new VariableType(lhsType.name, lhsType.dimension - 1, null,false);
        return null;
    }

    public Void visit(MemberAccess node){
        assert (ResolveTime == 2);
        node.scope = currentScope;

        visit(node.expr);
        VariableType exprType = (VariableType) node.expr.returnType;
        if (exprType.dimension > 0){
            if (node.name.equals("size")) node.returnType = new FunctionType("int", null);
            else except("the array type has no such members");
        }
        else {
            ClassDecl theClass = typeTable.get(exprType.name);
            if (!theClass.scope.hasKey(node.name)) except("The Class Has No Such Member");
            Type memberType = theClass.scope.get(node.name, null);
            if (memberType instanceof VariableType)
               node.memberOffset = theClass.scope.variableOffset.get(node.name);

            node.returnType =  memberType;
        }
        return null;
    }

    public Void visit(NewExpr node){
        assert ResolveTime == 2;

        node.scope = currentScope;
        if (node.size != null) node.size.forEach(x -> visit(x));
        for (Expr expr : node.size){
            if (!((VariableType)expr.returnType).checkInt()) except("not a int type in new in index");
        }

        node.returnType = new VariableType(node.temType.name, node.temType.dimension, null,false);
        return null;
    }

    public Void visit(PostUnaryExpr node){
        assert ResolveTime == 2;

        node.scope = currentScope;
        visit(node.expr);

        if (!((VariableType) node.expr.returnType).checkInt()) except("Not a Int type in PostUnaryExpr");
        if (((VariableType) node.expr.returnType).isConstant) except("in post unary, it should be a lvalue");
        node.returnType = new VariableType(node.expr.returnType.name, ((VariableType) node.expr.returnType).dimension, null,true);
        return null;
    }

    public Void visit(UnaryExpr node){
        assert ResolveTime == 2;

        node.scope = currentScope;
        visit(node.expr);

        VariableType tem = (VariableType) node.expr.returnType;

        switch(node.op){
            case PPLUS: case MMINUS:
                if (!(tem.name.equals("int") && tem.dimension == 0 && !tem.isConstant)) except("Wrong in ++--");
                node.returnType = new VariableType("int", 0, null,false);
                break;
            case PLUS:case SIM:case MINUS:
                if (!(tem.name.equals("int")  && tem.dimension == 0)) except("Wrong in +-~");
                node.returnType = new VariableType("int", 0, null,true);
                break;
            case BANG:
                if (!(tem.name.equals("bool") && tem.dimension == 0)) except("Wrong in !");
                node.returnType = new VariableType("bool", 0, null,true);
                break;
            default: except("Not a unary op!"); return null;
        }

        return null;
    }

    public Void visit(StringExpr node){
        assert ResolveTime == 2;

        node.scope = currentScope;
        node.returnType = new VariableType("string", 0, null,true);
        return null;
    }

    public Void visit(ThisExpr node){
        assert ResolveTime == 2;

        if (currentClassName == null) except("this occurs not in a class");
        node.className = currentClassName;
        node.scope = currentScope;

        node.returnType = new VariableType(currentClassName, 0, null,true);
    //    if (!node.scope.hasKey("this"))
      //      node.scope.put("this", node.returnType);
        return null;
    }

    public Void visit(BoolExpr node){
        assert ResolveTime == 2;
        node.scope = currentScope;
        node.returnType = new VariableType("bool", 0, null,true);
        return null;
    }

    public Void visit(NullExpr node){
        assert ResolveTime == 2;
        node.scope = currentScope;
        node.returnType = new VariableType("null", 0, null,true);
        return null;
    }

    public Void visit(IntExpr node){
        assert ResolveTime == 2;
        node.scope = currentScope;
        node.returnType = new VariableType("int", 0, null,true);
        return null;
    }

    public Void visit(LogicalExpr node){
        assert ResolveTime == 2;
        except("I think it is actually a boolexpr but not a logical expr");
        node.scope = currentScope;
        return null;
    }

    public Void visit(EmptyStmt node){
        assert ResolveTime == 2;
        node.scope = currentScope;
        return null;
    }
}